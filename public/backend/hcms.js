/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/backend/js/components/HandleLanguageCheckboxes.js":
/*!****************************************************************************!*\
  !*** ./resources/assets/backend/js/components/HandleLanguageCheckboxes.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HandleLanguageCheckboxes; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var HandleLanguageCheckboxes = /*#__PURE__*/function () {
  function HandleLanguageCheckboxes() {
    _classCallCheck(this, HandleLanguageCheckboxes);

    this.activeLangContainer = document.getElementById('hcms-language-checkboxes');
    this.allLangContainer = document.getElementById('hcms_all_languages');
    this.activeLangsDisablingCheckboxes = this.getActiveLangsDisablingCheckboxes();
    this.allLangDeleteButtons = this.getDeleteButtons();
    this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes();
    this.allLangCheckboxes = this.getAllLanguagesCheckboxes();
    this.updateButtonEnabled = false;
    this.mainLangSelected = true;
    this.updateButton = document.getElementById('hmcs-update-languages');
    this.activeLangRowPatternElement = this.createLanguageRowPatternElement();

    this.deleteLangButtonClicked = function (e) {
      var _this = this;

      var tableRow = e.target.closest('tr');
      var langId = tableRow.getAttribute('data-hcms-language-id');
      var allLangCheckbox = this.allLangCheckboxes.find(function (inputEl) {
        return inputEl.getAttribute('data-hcms-language-id') === langId;
      });
      allLangCheckbox.checked = false;
      tableRow.classList.add('fade');
      window.setTimeout(function () {
        tableRow.style.display = 'none';
        tableRow.remove();

        var activeLangCount = _this.activeLangContainer.querySelectorAll('tr[data-hcms-language-id]').length;

        document.getElementById('hcms_active_lang_count').innerHTML = activeLangCount;
      }, 300);
      this.enableUpdateButton();
    };

    this.handleLanguageDelete = this.deleteLangButtonClicked.bind(this);
    this.init();
  }

  _createClass(HandleLanguageCheckboxes, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      this.activeLangsDisablingCheckboxes.forEach(function (checkBoxEl) {
        checkBoxEl.addEventListener('click', function (e) {
          _this2.handleLanguageDisabling(e);
        });
      });
      this.mainLanguageCheckboxes.forEach(function (checkBoxEl) {
        checkBoxEl.addEventListener('click', function (e) {
          _this2.handleMainLanguageSelection(e);
        });
      });
      document.getElementById('all_lang_search').addEventListener('keyup', function (e) {
        _this2.handleAllLangSearchFilter(e);
      });
      this.allLangDeleteButtons.forEach(function (buttonEl) {
        if (!buttonEl.classList.contains('hcms-pointer-forbidden')) {
          buttonEl.addEventListener('click', _this2.handleLanguageDelete);
        }
      });
      this.allLangCheckboxes.forEach(function (checkBoxEl) {
        checkBoxEl.addEventListener('click', function (e) {
          _this2.handleLanguageAddClick(e);
        });
      });
    }
  }, {
    key: "createLanguageRowPatternElement",
    value: function createLanguageRowPatternElement() {
      var tableRow = this.activeLangContainer.querySelector('tr[data-hcms-language-id]').cloneNode(true);
      var deleteButton = tableRow.querySelector('svg[class="svg-delete hcms-pointer-forbidden"]');
      deleteButton.classList.remove('hcms-pointer-forbidden');
      return tableRow;
    }
  }, {
    key: "handleMainLanguageSelection",
    value: function handleMainLanguageSelection(e) {
      var deleteButton = e.target.closest('tr').querySelector('svg[class^="svg-delete"]');

      if (parseInt(e.target.value) === 1) {
        e.target.value = 0;
        e.target.checked = false;
        this.mainLangSelected = false;
        deleteButton.classList.remove('hcms-pointer-forbidden');
        deleteButton.addEventListener('click', this.handleLanguageDelete);
        this.mainLanguageCheckboxes.forEach(function (checkbox) {
          var activeLangDisablingCheckbox = checkbox.closest('tr').querySelector('input[data-hcms-lang-disabled=""]');

          if (activeLangDisablingCheckbox.checked === true) {
            return;
          }

          activeLangDisablingCheckbox.disabled = false;
          checkbox.disabled = false;
        });
        this.disableUpdateButton();
      } else {
        e.target.value = 1;
        e.target.checked = true;
        this.mainLangSelected = true;
        deleteButton.classList.add('hcms-pointer-forbidden');
        deleteButton.removeEventListener('click', this.handleLanguageDelete);
        this.mainLanguageCheckboxes.forEach(function (checkbox) {
          if (checkbox === e.target) {
            var activeLangDisablingCheckbox = checkbox.closest('tr').querySelector('input[data-hcms-lang-disabled=""]');
            activeLangDisablingCheckbox.disabled = true;
            return;
          }

          checkbox.disabled = true;
        });
        this.enableUpdateButton();
      }

      this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes();
    }
  }, {
    key: "handleLanguageAddClick",
    value: function handleLanguageAddClick(e) {
      var _this3 = this;

      var langId = e.target.getAttribute('data-hcms-language-id');
      var alreadyAdded = this.activeLangContainer.querySelector("tr[data-hcms-language-id=\"".concat(langId, "\"]"));

      if (alreadyAdded === null) {
        var langAlpha2Code = e.target.value;
        var langName = e.target.name;
        var clonedRow = this.activeLangRowPatternElement.cloneNode(true);
        clonedRow.setAttribute('data-hcms-language-id', langId);
        clonedRow.setAttribute('data-hcms-language-alpha_2', langAlpha2Code); // set the data from the activated lanuage to the cloned row
        // set flag icon

        clonedRow.getElementsByClassName('flag-icon')[0].className = "flag-icon flag-icon-".concat(langAlpha2Code); // set language name

        clonedRow.querySelector('p[id^="lang_name"]').innerHTML = langName; // give main lang checkbox correct id value & add eventlistener

        var mainLangCheckbox = clonedRow.querySelector('input[id^="main"]');
        mainLangCheckbox.setAttribute('id', "main_".concat(langId));

        if (this.mainLangSelected === true) {
          mainLangCheckbox.disabled = true;
        }

        mainLangCheckbox.value = 0;
        mainLangCheckbox.checked = false;
        mainLangCheckbox.addEventListener('click', function (e) {
          _this3.handleMainLanguageSelection(e);
        }); // update main lang checkbox label

        clonedRow.querySelector('label[for^="main"]').setAttribute('for', "main_".concat(langId)); // give lang disabling checkbox correct id, value & add eventlistener

        var activeLangDisablingLangCheckbox = clonedRow.querySelector('input[id^="disabled"]');
        activeLangDisablingLangCheckbox.setAttribute('id', "disabled_".concat(langId));
        activeLangDisablingLangCheckbox.value = 0;
        activeLangDisablingLangCheckbox.disabled = false;
        activeLangDisablingLangCheckbox.addEventListener('click', function (e) {
          _this3.handleLanguageDisabling(e);
        }); // update disable lang checkbox label

        clonedRow.querySelector('label[for^="disabled"]').setAttribute('for', "disabled_".concat(langId)); // add delete function to delete button

        clonedRow.querySelector('svg[name="delete"]').addEventListener('click', function (e) {
          _this3.handleLanguageDelete(e);
        });
        this.activeLangContainer.querySelector('tbody').append(clonedRow);
      } else {
        this.activeLangContainer.querySelector("tr[data-hcms-language-id=\"".concat(langId, "\"]")).remove();
      }

      var activeLangCount = this.activeLangContainer.querySelectorAll('tr[data-hcms-language-id]').length;
      document.getElementById('hcms_active_lang_count').innerHTML = activeLangCount;
      this.enableUpdateButton();
      this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes();
    }
  }, {
    key: "handleLanguageDisabling",
    value: function handleLanguageDisabling(e) {
      var tableRow = e.target.closest('tr');
      var mainLangCheckbox = tableRow.querySelector('input[data-hcms-main-language=""]');
      var mainLanguageChecked = this.isMainLangCheckboxChecked();

      if (parseInt(e.target.value) === 1) {
        e.target.value = 0;
        e.target.checked = false;
        tableRow.classList.remove('bg-light');
        tableRow.classList.remove('text-dark');

        if (mainLanguageChecked === false) {
          mainLangCheckbox.disabled = false;
        }

        if (parseInt(mainLangCheckbox.value) === 1) {
          this.activeLangContainer.classList.remove('border-warning');
        }
      } else {
        e.target.value = 1;
        e.target.checked = true;
        tableRow.classList.add('bg-light');
        tableRow.classList.add('text-dark');

        if (mainLanguageChecked === false) {
          mainLangCheckbox.disabled = true;
        }

        if (parseInt(mainLangCheckbox.value) === 1) {
          this.activeLangContainer.classList.add('border-warning');
        }
      }

      this.activeLangsDisablingCheckboxes = this.getActiveLangsDisablingCheckboxes();
      var leftLanguageInputs = [];
      this.activeLangsDisablingCheckboxes.forEach(function (element) {
        if (parseInt(element.value) === 0) {
          leftLanguageInputs.push(element);
        }
      });

      if (1 > leftLanguageInputs.length) {
        this.activeLangContainer.classList.add('border-danger');
        this.disableUpdateButton();
      }

      if (leftLanguageInputs.length >= 1) {
        this.activeLangContainer.classList.remove('border-danger');
        this.enableUpdateButton();
      }
    }
  }, {
    key: "getActiveLangsDisablingCheckboxes",
    value: function getActiveLangsDisablingCheckboxes() {
      return Array.from(this.activeLangContainer.querySelectorAll('input[data-hcms-lang-disabled=""]'));
    }
  }, {
    key: "getMainLanguageCheckboxes",
    value: function getMainLanguageCheckboxes() {
      return Array.from(this.activeLangContainer.querySelectorAll('input[data-hcms-main-language=""]'));
    }
  }, {
    key: "getAllLanguagesCheckboxes",
    value: function getAllLanguagesCheckboxes() {
      return Array.from(this.allLangContainer.querySelectorAll('input:not([id="all_lang_search"])'));
    }
  }, {
    key: "getDeleteButtons",
    value: function getDeleteButtons() {
      return Array.from(this.activeLangContainer.querySelectorAll('svg[name=delete]'));
    }
  }, {
    key: "handleAllLangSearchFilter",
    value: function handleAllLangSearchFilter(e) {
      var searchString = e.target.value.toUpperCase();

      for (var i = 0; i < this.allLangCheckboxes.length; i++) {
        var langName = this.allLangCheckboxes[i].name;

        if (langName.toUpperCase().indexOf(searchString) > -1) {
          this.allLangCheckboxes[i].parentNode.parentNode.style.display = '';
        } else {
          this.allLangCheckboxes[i].parentNode.parentNode.style.display = 'none';
        }
      }

      var visibleLangsCount = this.allLangContainer.querySelectorAll('div[name="hcms_lang_container"]:not([style="display: none;"])').length;
      var allLangCountElement = document.getElementById('hcms_all_lang_count');
      allLangCountElement.innerHTML = visibleLangsCount;
    }
  }, {
    key: "enableUpdateButton",
    value: function enableUpdateButton() {
      if (this.updateButtonEnabled === false && this.mainLangSelected === true) {
        this.updateButton.removeAttribute('disabled');
        this.updateButton.classList.remove('hcms-pointer-forbidden');
        this.updateButtonEnabled = true;
      }
    }
  }, {
    key: "disableUpdateButton",
    value: function disableUpdateButton() {
      if (this.updateButtonEnabled === true) {
        this.updateButton.setAttribute('disabled', 'disabled');
        this.updateButton.classList.add('hcms-pointer-forbidden');
        this.updateButtonEnabled = false;
      }
    }
  }, {
    key: "isMainLangCheckboxChecked",
    value: function isMainLangCheckboxChecked() {
      var checkedBox = [];
      this.mainLanguageCheckboxes.forEach(function (checkbox) {
        if (checkbox.checked === true) {
          checkedBox.push(checkbox);
        }
      });

      if (checkedBox.length > 0) {
        return true;
      }

      return false;
    }
  }]);

  return HandleLanguageCheckboxes;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/RemoveAlert.js":
/*!***************************************************************!*\
  !*** ./resources/assets/backend/js/components/RemoveAlert.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RemoveAlert; });
function RemoveAlert() {
  var alert = document.querySelector("div[class=alert-modal]");
  var warning = document.querySelector("div[class=warning-modal]");

  if (alert) {
    alert.style.display = "none";
  }

  if (warning) {
    warning.style.display = "none";
  }
}

/***/ }),

/***/ "./resources/assets/backend/js/hcms.js":
/*!*********************************************!*\
  !*** ./resources/assets/backend/js/hcms.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_RemoveAlert__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/RemoveAlert */ "./resources/assets/backend/js/components/RemoveAlert.js");
/* harmony import */ var _components_HandleLanguageCheckboxes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/HandleLanguageCheckboxes */ "./resources/assets/backend/js/components/HandleLanguageCheckboxes.js");


document.RemoveAlert = _components_RemoveAlert__WEBPACK_IMPORTED_MODULE_0__["default"];

if (document.body.contains(document.getElementById('hcms-language-checkboxes'))) {
  document.HandleLanguageCheckboxes = new _components_HandleLanguageCheckboxes__WEBPACK_IMPORTED_MODULE_1__["default"]();
}

/***/ }),

/***/ "./resources/assets/backend/sass/hcms.scss":
/*!*************************************************!*\
  !*** ./resources/assets/backend/sass/hcms.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/backend/sass/vendor/flags.scss":
/*!*********************************************************!*\
  !*** ./resources/assets/backend/sass/vendor/flags.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/frontend/sass/ExampleProject.scss":
/*!************************************************************!*\
  !*** ./resources/assets/frontend/sass/ExampleProject.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!****************************************************************************************************************************************************************************************************!*\
  !*** multi ./resources/assets/backend/js/hcms.js ./resources/assets/backend/sass/hcms.scss ./resources/assets/backend/sass/vendor/flags.scss ./resources/assets/frontend/sass/ExampleProject.scss ***!
  \****************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/leon/Projekte/hybridcms_dev/package/leonp5/hybridcms/resources/assets/backend/js/hcms.js */"./resources/assets/backend/js/hcms.js");
__webpack_require__(/*! /home/leon/Projekte/hybridcms_dev/package/leonp5/hybridcms/resources/assets/backend/sass/hcms.scss */"./resources/assets/backend/sass/hcms.scss");
__webpack_require__(/*! /home/leon/Projekte/hybridcms_dev/package/leonp5/hybridcms/resources/assets/backend/sass/vendor/flags.scss */"./resources/assets/backend/sass/vendor/flags.scss");
module.exports = __webpack_require__(/*! /home/leon/Projekte/hybridcms_dev/package/leonp5/hybridcms/resources/assets/frontend/sass/ExampleProject.scss */"./resources/assets/frontend/sass/ExampleProject.scss");


/***/ })

/******/ });