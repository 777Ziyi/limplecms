const mix = require('laravel-mix')
require('dotenv').config({ path: '../../../.env' })

let pathBackend
let pathFrontend

if (process.env.NODE_ENV === 'development') {
    mix.setPublicPath('../../../public')
    pathBackend = 'backend'
    pathFrontend = 'frontend'
}

const projectName = process.env.FRONTEND_NAME

if (process.env.NODE_ENV === 'production') {
    pathBackend = 'dist/backend'
    pathFrontend = 'dist/frontend'
}

mix.js('resources/assets/backend/js/hcms.js', pathBackend)
    .js('resources/assets/backend/js/hcms-vendor.js', pathBackend)
    .sass('resources/assets/backend/sass/hcms.scss', pathBackend)
    .sass('resources/assets/backend/sass/vendor/flags.scss', pathBackend)

mix.js(
    'resources/assets/frontend/js/ExampleProject.js',
    pathFrontend + '/' + projectName + '.js'
)
    .js(
        'resources/assets/frontend/js/ExampleProject-vendor.js',
        pathFrontend + '/' + projectName + '-vendor.js'
    )
    .sass(
        'resources/assets/frontend/sass/ExampleProject.scss',
        pathFrontend + '/' + projectName + '.css'
    )

mix.browserSync({
    proxy: 'http://127.0.0.1:8000',

    files: [
        'src/*.php',
        'src/**/*.php',
        'routes/*.php',
        'resources/**/*.php',
        'resources/assets/backend/js/*.js',
        'resources/assets/backend/js/**/*.js',
        'resources/assets/backend/sass/*.scss'
    ],
    // => true = the browser opens a new browser window with every npm run watch startup
    open: false,
    notify: false,
    ui: false
})
mix.disableNotifications()
