<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

use Leonp5\Hybridcms\Http\Controllers\HomeController;
use Leonp5\Hybridcms\Http\Controllers\Auth\LoginController;
use Leonp5\Hybridcms\Http\Controllers\Admin\PagesController;
use Leonp5\Hybridcms\Http\Controllers\Admin\UsersController;
use Leonp5\Hybridcms\Http\Controllers\Admin\LanguagesController;
use Leonp5\Hybridcms\Http\Controllers\FrontendController\FrontendController;

// Frontend

$cmsPrefix = config('hybridcms.cms_prefix');
$cmsUrl = '/' . $cmsPrefix;


// Route::get('{locale}', [FrontendController::class, 'receiveMultiLangRequest'])->where('locale', '[a-zA-Z]{2}')
//     ->name('frontend.multilang.start')->middleware('addlocale');

// Route::any('{locale}/{url}', [FrontendController::class, 'receiveMultiLangRequest'])->where(['url' => "^((?!${cmsPrefix}).)*?", 'locale' => '[a-zA-Z]{2}'])
//     ->name('frontend.multilang')->middleware('addlocale');

/**
 * The regex in the where clause excludes ONLY the 'cms' prefix string. If the string is something like 'foocms' or 'cmsfoo' 
 * the the request will be passed to the notMatchingUrl method
 * 
 *  */
Route::any('{url}', [FrontendController::class, 'receiveRequest'])
    ->name('frontend')->middleware('addlocale')->where('url', "^((?!(?<!\w)${cmsPrefix}(?!\w)).)*?");


// Backend (Cms)

// prefix all possible routes with /cms
Route::group(
    [
        'prefix' => $cmsUrl,
        'middleware' => 'lastonline'
    ],
    function () {

        // Set language
        Route::get('/locale/{locale}', function ($locale) {
            Session::put('locale', $locale);
            return redirect()->back();
        })->name('localization.backend');

        Route::get('/', [HomeController::class, 'welcome'])->name('welcome');
        Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');

        Route::get('/login', [LoginController::class, 'show'])->name('login');
        Route::post('/login', [LoginController::class, 'login']);
        Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

        /*
        * IMPORTANT: The url is used in the url validation of page creation and update process
        * 
        * Add, edit, move & delete pages
        */

        Route::resource(
            'pages',
            PagesController::class,
            ['except' => ['show']]
        )->middleware('auth');

        /**
         * Add, edit, delete users & their roles
         */
        Route::resource(
            'users',
            UsersController::class,
            ['except' => ['show']]
        )->middleware('can:admin');

        Route::resource(
            'languages',
            LanguagesController::class,
            ['except' => ['destroy, create, show']]
        )->middleware('can:admin');
    }
);
