<?php

namespace Leonp5\Hybridcms;

use Illuminate\Support\Arr;
use Illuminate\Routing\Router;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\View\Components\Svg;
use Leonp5\Hybridcms\Http\Middleware\SetLocale;
use Leonp5\Hybridcms\Http\Middleware\AddLocaleToUrl;
use Leonp5\Hybridcms\Http\Middleware\LastOnlineAt;
use Leonp5\Hybridcms\Providers\SeedsServiceProvider;

class HybridCmsProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'hybridcms');

        // merging this config file with the default Laravel auth config file
        $this->mergeConfigFrom(__DIR__ . '/../config/auth.php', 'auth');
    }

    public function boot(GateContract $gate)
    {
        $this->setMiddlewares();

        $this->registerRoutes();

        $this->registerGates($gate);

        $this->viewLoader();

        Paginator::useBootstrap();

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'hybridcms');

        $this->loadMigrationsFrom(__DIR__ . '/../database/Migrations');

        $this->app->register(SeedsServiceProvider::class);

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__ . '/../dist' => public_path('hybridcms'),
            ], 'assets');


            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('hybridcms.php'),
                __DIR__ . '/../config/auth.php' => config_path('auth_hybridcms.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../resources/views/frontend' => resource_path('views/vendor/hybridcms'),
            ], 'templates');
        }
    }

    /**
     * @return void 
     * @throws BindingResolutionException 
     */
    private function setMiddlewares()
    {
        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('web', SetLocale::class);
        // $router->pushMiddlewareToGroup('web', AddLocaleToUrl::class);
        $router->aliasMiddleware('addlocale', AddLocaleToUrl::class);
        $router->aliasMiddleware('lastonline', LastOnlineAt::class);
    }

    /**
     * @return void 
     * @throws BindingResolutionException 
     */
    private function viewLoader()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'hybridcms');

        $this->loadViewComponentsAs('hybridcms', [
            Svg::class,
        ]);
    }

    private function registerGates(GateContract $gate)
    {

        $gate->define("admin", function ($user) {
            return $user->hasRole("admin");
        });

        $gate->define("editor", function ($user) {
            return $user->hasRole("editor");
        });

        $gate->define("author", function ($user) {
            return $user->hasRole("author");
        });
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });
    }


    protected function routeConfiguration()
    {
        return [
            // 'prefix' => config('hybridcms.cms_prefix'),
            'middleware' => config('hybridcms.middleware'),
        ];
    }

    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param  string  $path
     * @param  string  $key
     * @return void
     */
    protected function mergeConfigFrom($path, $key)
    {
        $config = $this->app['config']->get($key, []);

        $this->app['config']->set($key, $this->mergeConfig(require $path, $config));
    }

    /**
     * Merges the configs together and takes multi-dimensional arrays into account.
     *
     * @param  array  $original
     * @param  array  $merging
     * @return array
     */
    protected function mergeConfig(array $original, array $merging)
    {
        $array = array_merge($original, $merging);

        foreach ($original as $key => $value) {
            if (!is_array($value)) {
                continue;
            }

            if (!Arr::exists($merging, $key)) {
                continue;
            }

            if (is_numeric($key)) {
                continue;
            }

            $array[$key] = $this->mergeConfig($value, $merging[$key]);
        }

        return $array;
    }
}
