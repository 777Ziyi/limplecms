<?php

namespace Leonp5\Hybridcms\View\Components;

use Illuminate\View\Component;

class Svg extends Component
{
    public $svg;
    public $width;
    public $height;
    public $viewBox;
    public $fill;
    public $strokeWidth;
    public $id;
    public $class;

    public function __construct(
        $svg = null,
        $width = 24,
        $height = 24,
        $viewBox = '24 24',
        $fill = 'currentColor',
        $strokeWidth = 2,
        $id = null,
        $class = null
    ) {
        $this->svg = $svg;
        $this->width = $width;
        $this->height = $height;
        $this->viewBox = $viewBox;
        $this->fill = $fill;
        $this->strokeWidth = $strokeWidth;
        $this->id = $id ?? '';
        $this->class = $class ?? '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('hybridcms::backend.components.svg');
    }
}
