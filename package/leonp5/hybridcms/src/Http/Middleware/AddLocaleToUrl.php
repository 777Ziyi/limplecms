<?php

namespace Leonp5\Hybridcms\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Leonp5\Hybridcms\Models\ActivatedLanguages;

use function strlen;

class AddLocaleToUrl
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $localeString =  $request->segment(1);
        $test3 = app()->getLocale();
        // App::setLocale($request->segment(1));
        URL::defaults(['locale' => app()->getLocale()]);
        $locale = App::getLocale();
        return $next($request);
    }
}
