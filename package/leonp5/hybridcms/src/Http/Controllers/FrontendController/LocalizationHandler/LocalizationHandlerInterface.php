<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\LocalizationHandler;


interface LocalizationHandlerInterface
{
    /**
     * @param LocalizationValidatorResponseTransfer $localizationValidatorResponseTransfer 
     * 
     * @return LocalizationValidatorResponseTransfer 
     */
    public function validate(
        LocalizationValidatorResponseTransfer $localizationValidatorResponseTransfer
    ): LocalizationValidatorResponseTransfer;

    /**
     * @param string $url
     * 
     * @return LocalizationValidatorResponseTransfer 
     */
    public function hasLocale(string $url): LocalizationValidatorResponseTransfer;
}
