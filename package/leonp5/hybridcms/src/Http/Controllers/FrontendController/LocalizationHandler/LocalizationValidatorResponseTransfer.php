<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\LocalizationHandler;

class LocalizationValidatorResponseTransfer
{

    /**
     * @var bool
     */
    private bool $success = true;

    /**
     * @var bool
     */
    private bool $hasLocale = false;

    /**
     * @var string
     */
    private string $locale;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var string
     */
    private string $message;

    /**
     * @return  bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param  bool  $success
     *
     * @return  LocalizationValidatorResponseTransfer
     */
    public function setSuccess(bool $success): LocalizationValidatorResponseTransfer
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return  string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param  string  $message
     *
     * @return  LocalizationValidatorResponseTransfer
     */
    public function setMessage(string $message): LocalizationValidatorResponseTransfer
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return  bool
     */
    public function hasLocale(): bool
    {
        return $this->hasLocale;
    }

    /**
     * @param  bool  $hasLocale
     *
     * @return  LocalizationValidatorResponseTransfer
     */
    public function setHasLocale(bool $hasLocale): LocalizationValidatorResponseTransfer
    {
        $this->hasLocale = $hasLocale;

        return $this;
    }

    /**
     * @return  string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param  string  $locale
     *
     * @return  LocalizationValidatorResponseTransfer
     */
    public function setLocale(string $locale): LocalizationValidatorResponseTransfer
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * 
     * @return LocalizationValidatorResponseTransfer
     */
    public function setUrl(string $url): LocalizationValidatorResponseTransfer
    {
        $this->url = $url;

        return $this;
    }
}
