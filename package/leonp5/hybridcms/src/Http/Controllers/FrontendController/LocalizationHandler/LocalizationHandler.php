<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\LocalizationHandler;

use Leonp5\Hybridcms\Models\ActivatedLanguages;
use Illuminate\Contracts\Container\BindingResolutionException;

use function trim;
use function preg_match;

class LocalizationHandler implements LocalizationHandlerInterface
{
    /**
     * @var array
     */
    private array $activatedLanguages;

    /**
     * @param LocalizationValidatorResponseTransfer $localizationValidatorResponseTransfer
     * 
     * @return LocalizationValidatorResponseTransfer
     * @throws BindingResolutionException
     */
    public function validate(
        LocalizationValidatorResponseTransfer $localizationValidatorResponseTransfer
    ): LocalizationValidatorResponseTransfer {

        $this->activatedLanguages = ActivatedLanguages::getActivatedLanguages();

        foreach ($this->activatedLanguages as $language) {
            if ($language->alpha_2 === $localizationValidatorResponseTransfer->getLocale()) {
                return $localizationValidatorResponseTransfer;
            }
        }

        $mainLanguage = ActivatedLanguages::getMainLanguage();
        $localizationValidatorResponseTransfer->setLocale($mainLanguage->alpha_2);

        return $localizationValidatorResponseTransfer;
    }

    /**
     * @param string $url 
     * 
     * @return LocalizationValidatorResponseTransfer 
     */
    public function hasLocale(string $url): LocalizationValidatorResponseTransfer
    {
        $localizationValidatorResponseTransfer = new LocalizationValidatorResponseTransfer();

        $regEx = '`^([a-zA-Z]{2})[/]`';
        preg_match($regEx, $url, $matches);
        if (!empty($matches)) {
            $localizationValidatorResponseTransfer->setHasLocale(true);
            $localizationValidatorResponseTransfer->setLocale(trim($matches[0], '/'));
            $localizationValidatorResponseTransfer->setUrl(
                substr($url, strpos($url, "/") + 1)
            );
        }
        return $localizationValidatorResponseTransfer;
    }
}
