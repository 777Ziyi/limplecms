<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\PageDataGetter;

use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Models\PageStatus;
use Leonp5\Hybridcms\Http\Controllers\FrontendController\Transfer\FrontendRequestTransfer;

class PageDataGetter implements PageDataGetterInterface
{

    /**
     * @param FrontendRequestTransfer $frontendRequestTransfer
     * 
     * @return DataGetterResponseTransfer
     */
    public function requestData(
        FrontendRequestTransfer $frontendRequestTransfer
    ): PageDataGetterResponseTransfer {
        $dataGetterResponseTransfer = new PageDataGetterResponseTransfer();
        $contentTable = 'content_' . $frontendRequestTransfer->getLocale();
        $url = '/' . $frontendRequestTransfer->getUrl();

        $requestedPage = DB::table($contentTable)
            ->where([$contentTable . '.url' => $url])
            ->join('pages', function ($join) use ($contentTable) {
                $join->on($contentTable . '.page_id', '=', 'pages.id')
                    ->where('pages.page_status_id', '=', PageStatus::PAGE_STATUS_PUBLISHED);
            })
            ->select([$contentTable . '.title', $contentTable . '.content', $contentTable . '.url'])
            ->first();

        if ($requestedPage === null) {
            $dataGetterResponseTransfer->setHasData(false);
            $dataGetterResponseTransfer->setLocale(
                $frontendRequestTransfer->getLocale()
            );
            return $dataGetterResponseTransfer;
        }

        $dataGetterResponseTransfer->setPageData($requestedPage);

        return $dataGetterResponseTransfer;
    }
}
