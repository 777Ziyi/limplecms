<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\PageDataGetter;

use Leonp5\Hybridcms\Http\Controllers\FrontendController\Transfer\FrontendRequestTransfer;

interface PageDataGetterInterface
{

    /**
     * @param FrontendRequestTransfer $frontendRequestTransfer
     * 
     * @return PageDataGetterResponseTransfer
     */
    public function requestData(
        FrontendRequestTransfer $frontendRequestTransfer
    ): PageDataGetterResponseTransfer;
}
