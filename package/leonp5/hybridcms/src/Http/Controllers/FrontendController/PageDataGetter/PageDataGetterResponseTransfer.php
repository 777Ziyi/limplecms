<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\PageDataGetter;


class PageDataGetterResponseTransfer
{
    /**
     * @var bool
     */
    private bool $hasData = true;

    /**
     * @var mixed
     */
    private mixed $pageData;

    /**
     * @var string
     */
    private string $locale;

    /**
     * @return  bool
     */
    public function hasData(): bool
    {
        return $this->hasData;
    }

    /**
     * @param  bool  $hasData
     *
     * @return  PageDataGetterResponseTransfer
     */
    public function setHasData(bool $hasData): PageDataGetterResponseTransfer
    {
        $this->hasData = $hasData;

        return $this;
    }

    /**
     * @return  mixed
     */
    public function getPageData()
    {
        return $this->pageData;
    }

    /**
     * @param  mixed  $pageData
     *
     * @return  PageDataGetterResponseTransfer
     */
    public function setPageData($pageData): PageDataGetterResponseTransfer
    {
        $this->pageData = $pageData;

        return $this;
    }

    /**
     * @return  string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param  string  $locale
     *
     * @return  PageDataGetterResponseTransfer
     */
    public function setLocale(string $locale): PageDataGetterResponseTransfer
    {
        $this->locale = $locale;

        return $this;
    }
}
