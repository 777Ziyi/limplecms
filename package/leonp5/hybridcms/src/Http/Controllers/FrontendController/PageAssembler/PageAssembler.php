<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\PageAssembler;

use Leonp5\Hybridcms\Http\Controllers\FrontendController\PageDataGetter\PageDataGetterResponseTransfer;

class PageAssembler implements PageAssemblerInterface
{

    /**
     * @param PageDataGetterResponseTransfer $pageDataGetterResponseTransfer
     * 
     * @return PageAssemblerResponseTransfer
     */
    public function assemble(
        PageDataGetterResponseTransfer
        $pageDataGetterResponseTransfer
    ): PageAssemblerResponseTransfer {

        $pageAssemblerResponseTransfer = new PageAssemblerResponseTransfer();
        if ($pageDataGetterResponseTransfer->hasData() !== true) {
            $pageAssemblerResponseTransfer->setPage(
                view('hybridcms::frontend.pages.404', [
                    'title' => '404',
                    'text' => __('hybridcms::frontend.page.not.found'),
                    'button' => __('hybridcms::frontend.page.not.found.button')
                ])
            );
            return $pageAssemblerResponseTransfer;
        };

        $pageAssemblerResponseTransfer->setPage(
            view('hybridcms::frontend.pages.start', [
                'title' => $pageDataGetterResponseTransfer->getPageData()->title,
            ])
        );

        return $pageAssemblerResponseTransfer;
    }
}
