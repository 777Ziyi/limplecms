<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\PageAssembler;

use Leonp5\Hybridcms\Http\Controllers\FrontendController\PageDataGetter\PageDataGetterResponseTransfer;

interface PageAssemblerInterface
{
    /**
     * @param PageDataGetterResponseTransfer $pageDataGetterResponseTransfer
     * 
     * @return PageAssemblerResponseTransfer
     */
    public function assemble(
        PageDataGetterResponseTransfer
        $pageDataGetterResponseTransfer
    ): PageAssemblerResponseTransfer;
}
