<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\PageAssembler;

class PageAssemblerResponseTransfer
{
    /**
     * @var mixed
     */
    private mixed $page;

    /**
     * @return  mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param  mixed  $page
     *
     * @return  PageAssemblerResponseTransfer
     */
    public function setPage($page): PageAssemblerResponseTransfer
    {
        $this->page = $page;

        return $this;
    }
}
