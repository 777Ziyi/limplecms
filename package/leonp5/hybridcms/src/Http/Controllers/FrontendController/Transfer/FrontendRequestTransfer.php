<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController\Transfer;


class FrontendRequestTransfer
{

    /**
     * @var string
     */
    private string $url;

    /**
     * @var string
     */
    private string $locale;

    /**
     * @var bool
     */
    private bool $hasLocale = true;

    /**
     * @var string
     */
    private string $method;

    /**
     * @param string $url 
     * @param string $locale 
     * @return void 
     */
    public function __construct(
        string $url,
        string $method,
        string $locale
    ) {
        $this->url = $url;
        $this->method = $method;
        $this->locale = $locale;
    }

    /**
     * @param string $locale 
     * 
     * @return FrontendRequestTransfer 
     */
    public function setLocale(string $locale): FrontendRequestTransfer
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $url
     * 
     * @return FrontendRequestTransfer
     */
    public function setUrl(string $url): FrontendRequestTransfer
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return  string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return  string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return  bool
     */
    public function hasLocale(): bool
    {
        return $this->hasLocale;
    }

    /**
     * @param  bool  $hasLocale
     *
     * @return  FrontendRequestTransfer
     */
    public function setHasLocale(bool $hasLocale): FrontendRequestTransfer
    {
        $this->hasLocale = $hasLocale;

        return $this;
    }
}
