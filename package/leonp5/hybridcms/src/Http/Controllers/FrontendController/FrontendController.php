<?php

namespace Leonp5\Hybridcms\Http\Controllers\FrontendController;

use Illuminate\Http\Request;

use Leonp5\Hybridcms\Http\Controllers\Controller;
use Leonp5\Hybridcms\Http\Controllers\FrontendController\PageAssembler\PageAssembler;
use Leonp5\Hybridcms\Http\Controllers\FrontendController\PageDataGetter\PageDataGetter;
use Leonp5\Hybridcms\Http\Controllers\FrontendController\Transfer\FrontendRequestTransfer;
use Leonp5\Hybridcms\Http\Controllers\FrontendController\LocalizationHandler\LocalizationHandler;

class FrontendController extends Controller
{
    public function receiveRequest(Request $request)
    {
        if ($request->url === null) {
            $request->url = '/';
        }


        $frontendRequestTransfer = new FrontendRequestTransfer(
            $request->url,
            $request->method(),
            app()->getLocale()
        );

        if (env('FRONTEND_MULTILANG') === true) {
            $localizationValidator = new LocalizationHandler();
            $localizationValidatorResponseTransfer = $localizationValidator
                ->hasLocale($frontendRequestTransfer->getUrl());
            if ($localizationValidatorResponseTransfer->hasLocale() == true) {
                $localizationValidatorResponseTransfer = $localizationValidator->validate(
                    $localizationValidatorResponseTransfer
                );
                $frontendRequestTransfer->setLocale(
                    $localizationValidatorResponseTransfer->getLocale()
                );
                $frontendRequestTransfer->setUrl(
                    $localizationValidatorResponseTransfer->getUrl()
                );
            }
        }
        $dataGetter = new PageDataGetter();
        $dataGetterResponseTransfer = $dataGetter->requestData($frontendRequestTransfer);
        $pageAssembler = new PageAssembler();
        $pageAssemblerResponseTranser = $pageAssembler->assemble($dataGetterResponseTransfer);

        return $pageAssemblerResponseTranser->getPage();
    }

    public function notMatchingUrl(Request $request)
    {
        $test = [];
        $test['url'] = $request->url;
        $test['locale'] = $request->locale;
        $test['faulty'] = 'faulty';

        dd($test);
        dd($request);

        // return view()
    }
}
