<?php

namespace Leonp5\Hybridcms\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        return view('hybridcms::backend.user.index');
    }

    public function welcome()
    {
        return view('hybridcms::backend.welcome');
    }
}
