<?php

namespace Leonp5\Hybridcms\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

use Leonp5\Hybridcms\Models\User;
use Leonp5\Hybridcms\Http\Controllers\Controller;
use Leonp5\Hybridcms\Http\Controllers\Admin\Transfer\ResponseTransfer;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUsers = DB::table('users')
            ->select(['users.id', 'users.uuid', 'users.username', 'users.last_online_at'])
            ->get();

        $roleUserLinks = DB::table('role_user')
            ->join('roles', 'roles.id', '=', 'role_user' . '.role_id')
            ->select(['role_user.role_id', 'role_user.user_id', 'roles.role'])
            ->get();

        foreach ($allUsers as $user) {
            $user->last_online_at = Carbon::createFromFormat('Y-m-d H:i:s', $user->last_online_at)->translatedFormat('D, j.m.y, H:i');
            $roles = [];
            foreach ($roleUserLinks as $userRole) {
                if ($user->id === $userRole->user_id) {
                    $roles[] = __('hybridcms::admin.user.role.' . $userRole->role);
                }
            }
            $user->roles = implode(', ', $roles);
        }

        return view('hybridcms::backend.admin.users.index', ['allUsers' => $allUsers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $availableRoles = DB::table('roles')
            ->get(['roles.id', 'roles.role'])
            ->sortBy('role');

        return view('hybridcms::backend.admin.users.create', ['roles' => $availableRoles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = new ResponseTransfer;
        $userOperator = new User();
        $currentUser = Auth::User();

        $userOperator->validateUserName($request, $response);
        $userOperator->validateRoleSelection($request, $response, $currentUser->uuid);
        $userOperator->validatePassword(
            $request->input('password'),
            $request->input('cpassword'),
            $response
        );


        if ($response->getSuccess() === false) {
            $request->session()->flash("error", $response->getMessage());
            return redirect()->back()->withInput(
                $request->except('password', 'cpassword')
            );
        }

        $userOperator->addUser($request, $response);

        if ($response->getSuccess() === false) {
            $request->session()->flash("error", $response->getMessage());
            return redirect()->back();
        }

        return redirect()->route('users.index')->with('success', $response->getMessage());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($userUuid)
    {
        $user = DB::table('users')
            ->select(['users.id', 'users.uuid', 'users.username'])
            ->where('users.uuid', '=', $userUuid)
            ->first();

        $roles = DB::table('roles')
            ->select(['roles.id', 'roles.role'])
            ->get()
            ->sortBy('role');

        $userRoles = DB::table('role_user')
            ->select(['role_user.role_id', 'role_user.user_id'])
            ->where('role_user.user_id', '=', $user->id)
            ->get();

        foreach ($userRoles as $userRole) {
            foreach ($roles as $role) {
                if ($role->id === $userRole->role_id) {
                    $role->userHas = true;
                }
            }
        }

        return view('hybridcms::backend.admin.users.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Leonp5\Hybridcms\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $response = new ResponseTransfer;
        $userOperator = new User();
        $currentUserUuid = Auth::User()->uuid;

        $userOperator->validateUserName($request, $response, $currentUserUuid);
        $userOperator->validateRoleSelection($request, $response, $currentUserUuid);
        $userOperator->validatePassword(
            $request->input('password'),
            $request->input('cpassword'),
            $response
        );

        if ($response->getSuccess() === false) {
            $request->session()->flash("error", $response->getMessage());
            return redirect()->back()->withInput(
                $request->except('password', 'cpassword')
            );
        }

        $userOperator->updateUser($request, $user, $response);

        return redirect()->route('users.index')->with('success', $response->getMessage());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::table('users')->where('id', $user->id)->delete();
        DB::table('role_user')->where('user_id', $user->id)->delete();

        return redirect()->route('users.index')
            ->with('success', __('hybridcms::admin.delete.user.success', ['username' => $user->username]));
    }
}
