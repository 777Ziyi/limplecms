<?php

namespace Leonp5\Hybridcms\Http\Controllers\Admin\Transfer;

class ResponseTransfer
{
    /**
     * @var bool
     */
    private bool $success;

    /**
     * @var string
     */
    private string $message;

    public function __construct()
    {
        $this->success = true;
    }

    /**
     * Get the value of success
     *
     * @return  bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * Set the value of success
     *
     * @param  bool  $success
     *
     * @return  self
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get the value of message
     *
     * @return  string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @param  string  $message
     *
     * @return  self
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }
}
