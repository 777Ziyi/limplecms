<?php

namespace Leonp5\Hybridcms\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Models\Page;
use Leonp5\Hybridcms\Models\Content;
use Leonp5\Hybridcms\Models\ActivatedLanguages;
use Leonp5\Hybridcms\Http\Controllers\Controller;
use Leonp5\Hybridcms\Http\Controllers\Admin\Transfer\ResponseTransfer;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPages = [];

        $mainLanguage = ActivatedLanguages::getMainLanguage();

        $lang = $mainLanguage->alpha_2;

        $contentTable = 'content_' . $lang;
        $pageStatusLang = 'page_statuses_' . $lang;

        $allPages = DB::table('pages')
            ->join($pageStatusLang, 'pages.page_status_id', '=', $pageStatusLang . '.page_status_id')
            ->leftJoin($contentTable, 'pages.id', '=', $contentTable . '.page_id')
            ->select(['pages.id', 'pages.user_id', 'pages.available_lang', $contentTable . '.title', $contentTable . '.url', $pageStatusLang . '.status_label', $pageStatusLang . '.page_status_id'])
            ->paginate(15);

        return view('hybridcms::backend.user.pages.index', ['allPages' => $allPages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang = App::getLocale();

        $pageStatusLangTable = 'page_statuses_' . $lang;

        $statusLabels = DB::table($pageStatusLangTable)
            ->select([$pageStatusLangTable . '.id', $pageStatusLangTable . '.status_label'])
            ->get();

        $activeLanguages = ActivatedLanguages::getActivatedLanguages();

        return view('hybridcms::backend.user.pages.create', ['statusLabels' => $statusLabels, 'activeLanguages' => $activeLanguages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = new ResponseTransfer();

        $contentLanguageCodes = $request->input('languages');

        $content = new Content();
        // check if exists at least one title
        $content->validateTitle($request, $response, $contentLanguageCodes);
        // check if url is valid and unique
        $content->validateUrl($request, $response, $contentLanguageCodes);

        if (false === $response->getSuccess()) {

            return back()->withInput()->with('error', $response->getMessage());
        }

        $content->insertContent($request, $response, $contentLanguageCodes);

        if (true !== $response->getSuccess()) {

            return back()->with('error', $response->getMessage());
        }
        return redirect()->route('pages.index')->with('success', __('hybridcms::admin.page.save.success'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $lang = App::getLocale();
        $pageStatusLang = 'page_statuses_' . $lang;

        $pageToEdit = DB::table('pages')
            ->where('pages.id', '=', $page->id)
            ->join($pageStatusLang, $pageStatusLang . '.page_status_id', '=', 'pages.page_status_id')
            ->select([
                'pages.id', 'pages.user_id', 'pages.available_lang',
                $pageStatusLang . '.status_label', $pageStatusLang . '.page_status_id',
            ])
            ->first();

        $page = new Page();
        $page->createPageWithAllLang($pageToEdit);

        $status = DB::table($pageStatusLang)
            ->where($pageStatusLang . '.page_status_id', '!=', $pageToEdit->page_status_id)
            ->get();

        return view('hybridcms::backend.user.pages.edit', ['page' => $pageToEdit, 'statusLabels' => $status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $response = new ResponseTransfer();

        $contentLanguageCodes = $request->input('languages');

        $content = new Content();
        // check if exists at least one title
        $content->validateTitle($request, $response, $contentLanguageCodes);
        // check if url is valid and unique
        $content->validateUrlForUpdate($request, $response, $contentLanguageCodes, $page);

        if (true !== $response->getSuccess()) {

            return back()->withInput()->with('error', $response->getMessage());
        }

        $content->updateContent($request, $contentLanguageCodes, $page);

        return redirect()->route('pages.index')->with('success', __('hybridcms::admin.page.update.success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        DB::table('pages')->where('id', $page->id)->delete();

        $activeLanguages = ActivatedLanguages::getActivatedLanguages();

        foreach ($activeLanguages as $activeLanguage) {

            $contentTable = 'content_' . $activeLanguage->alpha_2;

            DB::table($contentTable)->where('page_id', $page->id)->delete();
        }

        return redirect()->route('pages.index')->with('success', __('hybridcms::admin.page.delete.success'));
    }
}
