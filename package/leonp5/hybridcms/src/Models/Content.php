<?php

namespace Leonp5\Hybridcms\Models;

use Carbon\Carbon;
use RuntimeException;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Http\Controllers\Admin\Transfer\ResponseTransfer;

class Content extends Model
{
    use HasFactory;

    /**
     * @param Request $request 
     * @param ResponseTransfer $response 
     * @param array $contentLanguageCodes
     * 
     * @return ResponseTransfer 
     * @throws InvalidArgumentException 
     */
    public function insertContent(
        Request $request,
        ResponseTransfer $response,
        array $contentLanguageCodes
    ) {
        $availableLanguages = [];

        $currentUserId = Auth::user()->id;
        $pageStatusId = $request->input('page_status');

        $pageId = DB::table('pages')->insertGetId(
            [
                'user_id' => $currentUserId,
                'page_status_id' => $pageStatusId,
                'created_at' => Carbon::now()
            ]
        );

        // save the inputs for each language
        foreach ($contentLanguageCodes as $languageCode) {
            $title = $request->input('title_' . $languageCode);
            $url = $request->input('url_' . $languageCode);
            $content = $request->input('content_' . $languageCode);

            $valuesToCheck = [$title, $url, $content];

            // if one value is NOT null make a DB entry
            if (in_array(!null, $valuesToCheck)) {
                // if no URL exists, make the page to a draft
                if (null === $url) {
                    $pageStatusId = 2;
                }

                DB::table('content_' . $languageCode)->insert(
                    [
                        'page_id' => $pageId,
                        'title' => $title,
                        'url' => $url,
                        'content' => $content,
                    ]
                );

                $availableLanguages[] = $languageCode;
            }
        }

        $languagesString = implode(', ', $availableLanguages);

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'available_lang' => $languagesString,
            ]);

        return $response;
    }

    /**
     * @param Request $request
     * @param array $contentLanguageCodes
     * @param Page $page
     * 
     * @return void 
     * @throws InvalidArgumentException 
     */
    public function updateContent(
        Request $request,
        array $contentLanguageCodes,
        Page $page
    ) {
        $availableLanguages = [];

        $currentUserId = Auth::user()->id;
        $pageStatusId = $request->input('page_status');
        $pageId = $page->id;

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'user_id' => $currentUserId,
                'page_status_id' => $pageStatusId,
                'updated_at' => Carbon::now()
            ]);

        // save the inputs for each language
        foreach ($contentLanguageCodes as $languageCode) {
            $title = $request->input('title_' . $languageCode);
            $url = $request->input('url_' . $languageCode);
            $content = $request->input('content_' . $languageCode);

            $valuesToCheck = [$title, $url, $content];

            // if one value is NOT null make a DB entry
            if (in_array(!null, $valuesToCheck)) {
                // if no URL exists, make the page to a draft
                if (null === $url) {
                    $pageStatusId = 2;
                }

                DB::table('content_' . $languageCode)
                    ->updateOrInsert(
                        ['page_id' => $pageId],
                        [
                            'title' => $title,
                            'url' => $url,
                            'content' => $content,
                        ]
                    );

                $availableLanguages[] = $languageCode;
            }
        }

        $languagesString = implode(', ', $availableLanguages);

        DB::table('pages')
            ->where('id', $pageId)
            ->update([
                'available_lang' => $languagesString,
            ]);
    }

    /**
     * @param Request $request
     * @param ResponseTransfer $response
     * @param array $contentLanguages
     * 
     * @return ResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function validateTitle(
        Request $request,
        ResponseTransfer $response,
        array $contentLanguages
    ): ResponseTransfer {
        $existAtLeastOneTitle = false;

        foreach ($contentLanguages as $language) {
            $title = $request->input('title_' . $language);
            if (null !== $title) {
                $existAtLeastOneTitle = true;
            }
        }

        if (false === $existAtLeastOneTitle) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.page.missing.title.min');
            $response->setMessage($msg);

            return $response;
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param ResponseTransfer $response
     * @param array $contentLanguageCodes
     * 
     * @return ResponseTransfer
     * @throws BindingResolutionException
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function validateUrl(
        Request $request,
        ResponseTransfer $response,
        array $contentLanguageCodes
    ): ResponseTransfer {
        $atLeastOneValidUrl = false;

        $urlsInsidePageDiffer = $this->validateDifferenceOfUrlsInsideThePage($response, $request, $contentLanguageCodes);

        if (true === $urlsInsidePageDiffer) {
            $activatedLanguages = ActivatedLanguages::getActivatedLanguages();

            foreach ($contentLanguageCodes as $languageCode) {
                $url = $request->input('url_' . $languageCode);

                if (null !== $url) {
                    $validatedUrl = $this->checkForSlashesAndWhiteSpaces($url);

                    if (false === $validatedUrl) {
                        $response->setSuccess(false);
                        $msg = __('hybridcms::admin.invalid.url');
                        $response->setMessage($msg);

                        return $response;
                    }

                    // check if URL is unique by looking in every content table
                    foreach ($activatedLanguages as $activeLanguage) {
                        $languageCode = $activeLanguage->alpha_2;

                        $contentTable = $this->createContentTable($languageCode);

                        if (DB::table($contentTable)->where($contentTable . '.url', '=', $url)->exists()) {
                            $response->setSuccess(false);
                            $msg = trans('hybridcms::admin.url.already.exists', ['language' => $languageCode]);
                            $response->setMessage($msg);

                            return $response;
                        }
                    }

                    $atLeastOneValidUrl = true;
                }
            }

            if (false === $atLeastOneValidUrl) {
                $response->setSuccess(false);
                $msg = __('hybridcms::admin.min.one.url');
                $response->setMessage($msg);

                return $response;
            }
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param ResponseTransfer $response
     * @param array $contentLanguageCodes
     * @param Page $pageToUpdate
     * 
     * @return ResponseTransfer
     * @throws BindingResolutionException
     * @throws InvalidArgumentException
     */
    public function validateUrlForUpdate(
        Request $request,
        ResponseTransfer $response,
        array $contentLanguageCodes,
        Page $pageToUpdate
    ): ResponseTransfer {
        $atLeastOneValidUrl = false;

        $urlsInsidePageDiffer = $this->validateDifferenceOfUrlsInsideThePage($response, $request, $contentLanguageCodes);

        if (true === $urlsInsidePageDiffer) {
            $activatedLanguages = ActivatedLanguages::getActivatedLanguages();

            foreach ($contentLanguageCodes as $languageCode) {
                $url = $request->input('url_' . $languageCode);

                if (null !== $url) {
                    $validatedUrl = $this->checkForSlashesAndWhiteSpaces($url);

                    if (false === $validatedUrl) {
                        $response->setSuccess(false);
                        $msg = __('hybridcms::admin.invalid.url');
                        $response->setMessage($msg);

                        return $response;
                    }

                    // check if URL is unique by looking in every content table
                    foreach ($activatedLanguages as $activeLanguage) {
                        $languageCode = $activeLanguage->alpha_2;

                        $contentTable = $this->createContentTable($languageCode);

                        $pageWithUrl = DB::table($contentTable)->where($contentTable . '.url', '=', $url)->first();

                        // if the url exists and it is not the one in the page which will be updated throw the error
                        if (null !== $pageWithUrl && $pageWithUrl->page_id !== $pageToUpdate->id) {
                            $response->setSuccess(false);
                            $msg = trans('hybridcms::admin.url.already.exists', ['language' => $languageCode]);
                            $response->setMessage($msg);

                            return $response;
                        }
                    }

                    $atLeastOneValidUrl = true;
                }
            }

            if (false === $atLeastOneValidUrl) {
                $response->setSuccess(false);
                $msg = __('hybridcms::admin.min.one.url');
                $response->setMessage($msg);

                return $response;
            }
        }

        return $response;
    }

    /**
     * @param ResponseTransfer $response
     * @param Request $request
     * @param array $contentLanguageCodes
     * 
     * @return bool 
     * @throws BindingResolutionException
     */
    private function validateDifferenceOfUrlsInsideThePage(
        ResponseTransfer $response,
        Request $request,
        array $contentLanguageCodes
    ): bool {
        $urls = [];

        foreach ($contentLanguageCodes as $languageCode) {
            $url = $request->input('url_' . $languageCode);
            if (null !== $url) {
                $urls[] = $url;
            }
        }

        $unique = array_unique($urls);
        $duplicates = array_diff_key($urls, $unique);
        if ([] !== $duplicates) {
            $response->setSuccess(false);
            $msg = trans('hybridcms::admin.page.need.different.urls');
            $response->setMessage($msg);

            return false;
        }

        return true;
    }

    /**
     * @param string $url
     * 
     * @return bool
     *
     * @throws BindingResolutionException
     */
    private function checkForSlashesAndWhiteSpaces(string $url): bool
    {
        $currentUrl = url()->current();
        $urlToCheck = $currentUrl . $url;

        $urlStartsWithSlash = '/' === substr($url, 0, strlen('/'));
        $urlHasOnlyOneSlash = '/' === substr($url, 1, strlen('/'));
        $urlHasWhiteSpace = preg_match('/\s/', $url);
        $validUrl = parse_url($urlToCheck, PHP_URL_SCHEME) && parse_url($urlToCheck, PHP_URL_HOST);

        if (
            true === $urlStartsWithSlash
            && false === $urlHasOnlyOneSlash
            && 0 === $urlHasWhiteSpace
            && true === $validUrl
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param string $language
     * 
     * @return string
     */
    private function createContentTable(string $language): string
    {
        return 'content_' . $language;
    }
}
