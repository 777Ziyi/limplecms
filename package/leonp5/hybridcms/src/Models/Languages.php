<?php

namespace Leonp5\Hybridcms\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Collection;

class Languages extends Model
{

    public const PAGE_STATUS_PUBLISHED = 1;
    public const PAGE_STATUS_DRAFT = 2;
    public const PAGE_STATUS_INACTIVE = 3;

    use HasFactory;

    /**
     * @return Collection 
     */
    public static function findAll()
    {
        $langTable = 'language_' . App::getLocale();

        return DB::table($langTable)
            ->get();
    }
}
