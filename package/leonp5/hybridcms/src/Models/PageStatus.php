<?php

namespace Leonp5\Hybridcms\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PageStatus extends Model
{

    public const PAGE_STATUS_PUBLISHED = 1;
    public const PAGE_STATUS_DRAFT = 2;
    public const PAGE_STATUS_INACTIVE = 3;

    use HasFactory;

    /**
     * 
     * @return BelongsToMany 
     */
    public function pages(): BelongsToMany
    {
        return $this->belongsToMany('Leonp5\Hybridcms\Models\Page');
    }
}
