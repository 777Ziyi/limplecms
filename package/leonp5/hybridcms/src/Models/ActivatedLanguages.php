<?php

namespace Leonp5\Hybridcms\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use InvalidArgumentException;

class ActivatedLanguages extends Model
{
    use HasFactory;

    /**
     * @return array 
     * @throws InvalidArgumentException 
     */
    public static function getActivatedLanguages(): array
    {
        // getting the language name in the correct language
        $langTable = 'language_' . App::getLocale();

        $activeLanguages = [];

        $activatedLanguages = DB::table('activated_languages')
            ->select(['activated_languages.lang_id', 'activated_languages.main_language', 'activated_languages.disabled'])
            ->where('activated_languages.disabled', '=', false)
            ->get();

        foreach ($activatedLanguages as $language) {

            $activeLanguage = DB::table($langTable)
                ->select([$langTable . '.name as langName', $langTable . '.id', $langTable . '.alpha_2'])
                ->where($langTable . '.id', '=', $language->lang_id)
                ->first();

            $activeLanguages[] = (object) array_merge((array)$activeLanguage, (array)$language);
        }

        return $activeLanguages;
    }

    /**
     * @return array 
     * @throws InvalidArgumentException 
     */
    public static function withDisabled(): array
    {
        // getting the language name in the correct language
        $langTable = 'language_' . App::getLocale();

        $activeLanguages = [];

        $activatedLanguages = DB::table('activated_languages')
            ->select(['activated_languages.lang_id', 'activated_languages.main_language', 'activated_languages.disabled'])
            ->get();

        foreach ($activatedLanguages as $language) {

            $activeLanguage = DB::table($langTable)
                ->select([$langTable . '.name as langName', $langTable . '.id', $langTable . '.alpha_2'])
                ->where($langTable . '.id', '=', $language->lang_id)
                ->first();

            $activeLanguages[] = (object) array_merge((array)$activeLanguage, (array)$language);
        }

        return $activeLanguages;
    }


    /**
     * @return object|null 
     * @throws InvalidArgumentException 
     */
    public static function getMainLanguage()
    {
        $mainLanguage = DB::table('activated_languages')
            ->select(['activated_languages.alpha_2'])
            ->where('activated_languages.main_language', '=', 1)
            ->first();

        return $mainLanguage;
    }
}
