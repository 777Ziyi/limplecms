<?php

namespace Leonp5\Hybridcms\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Page extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('Leonp5\Hybridcms\Models\User');
    }

    /**
     * 
     * @return BelongsToMany 
     */
    public function status(): BelongsToMany
    {
        return $this->belongsToMany('Leonp5\Hybridcms\Models\PageStatus');
    }

    /**
     *  @return void
     */
    public function createPageWithAllLang($page): void
    {
        $activeLanguages = ActivatedLanguages::getActivatedLanguages();
        $availableLanguages = explode(', ', $page->available_lang);

        foreach ($activeLanguages as $activeLanguage) {

            $languageCode = $activeLanguage->alpha_2;

            // make a DB request only when the page exists in the language
            if (in_array($languageCode, $availableLanguages)) {

                $contentTable = $this->createContentTable($languageCode);

                if ($languageCode === 'en') {
                    $languageCode = 'gb';
                }

                $content = DB::table('pages')
                    ->join($contentTable, 'pages.id', '=', $contentTable . '.page_id')
                    ->where($contentTable . '.page_id', '=', $page->id)
                    ->select([
                        $contentTable . '.title', $contentTable . '.url',
                        $contentTable . '.content'
                    ])
                    ->first();

                if ($languageCode === 'gb') {
                    $languageCode = 'en';
                }

                $content->lang = $languageCode;
                $content->langName = $activeLanguage->langName;

                $page->langs[] = $content;
            } else {
                // "fake" the $content object for the languages in which the page doesnt exist
                $content = (object) [
                    'lang' => $languageCode,
                    'langName' => $activeLanguage->langName,
                    'content' => null,
                    'title' => null,
                    'url' => null
                ];

                $page->langs[] = $content;
            }
        }
    }

    /**
     * @param string $language 
     * @return string 
     */
    private function createContentTable(string $language): string
    {
        if ($language === 'gb')
            $language = 'en';

        $contentTable = 'content_' . $language;

        return $contentTable;
    }
}
