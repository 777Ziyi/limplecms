<?php

namespace Leonp5\Hybridcms\Models;

use Throwable;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Http\Controllers\Admin\Transfer\ResponseTransfer;
use Leonp5\Hybridcms\Helper\PasswordValidation\PasswordValidationHelper;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        "last_online_at" => "datetime"
    ];

    /**
     * 
     * @return HasMany 
     */
    public function pages(): HasMany
    {
        return $this->hasMany('Leonp5\Hybridcms\Models\Page');
    }

    /**
     * 
     * @return BelongsToMany 
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany('Leonp5\Hybridcms\Models\Role');
    }

    /**
     * 
     * @param mixed $roles 
     * @return bool 
     */
    public function hasAnyRole($roles): bool
    {
        return null !== $this->roles()
            ->whereIn('role', $roles)->first();
    }

    /**
     * 
     * @param mixed $role 
     * @return bool 
     */
    public function hasRole($role): bool
    {
        return null !== $this->roles()
            ->where('role', $role)->first();
    }

    /**
     *
     * @return string 
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * @param Request $request 
     * @param ResponseTransfer $response 
     * @return ResponseTransfer 
     */
    public function addUser(
        Request $request,
        ResponseTransfer $response
    ): ResponseTransfer {

        try {
            $userId = DB::table('users')
                ->insertGetId(
                    [
                        'password' => Hash::make($request->input('password')),
                        'username' => $request->input('username'),
                        'uuid' => Str::uuid(),
                        'created_At' => Carbon::now()
                    ]
                );
        } catch (Throwable $e) {
            $response->setSuccess(false);
            $response->setMessage($e);
            return $response;
        }

        $roleData = [];
        foreach ($request->input('roles') as $key => $value) {
            $roleData[] = [
                'role_id' => $value,
                'user_id' => $userId
            ];
        }

        DB::table('role_user')
            ->insert($roleData);

        $msg = __('hybridcms::admin.save.user.success');
        $response->setMessage($msg);

        return $response;
    }

    /**
     * @param Request $request 
     * @param User $user 
     * @param ResponseTransfer $response 
     * @return ResponseTransfer 
     */
    public function updateUser(
        Request $request,
        User $user,
        ResponseTransfer $response
    ): ResponseTransfer {

        $user->roles()->sync($request->input("roles"));

        //Save user
        if ($request->input("username")) {
            $user->username = $request->input("username");
        }

        if ($request->input('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        if ($user->save()) {
            $response->setMessage(__('hybridcms::admin.update.data.success'));
        };

        return $response;
    }

    /**
     * @param string $username 
     * @param ResponseTransfer $response 
     * @return ResponseTransfer|void 
     * @throws BindingResolutionException 
     */
    public function validateUserName(
        Request $request,
        ResponseTransfer $response,
    ): ResponseTransfer {

        $newUsername = $request->input('username');
        $requestedUserUuid = $request->input('user_uuid');
        $minLength = 3;

        if ($newUsername === null) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.short.name.error', ['minLength' => $minLength]);
            $response->setMessage($msg);
            return $response;
        }

        if (strlen($newUsername) < $minLength) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.short.name.error', ['minLength' => $minLength]);
            $response->setMessage($msg);
            return $response;
        }

        $userCollection = DB::table('users')
            ->select(['users.username', 'users.uuid'])
            ->get();

        foreach ($userCollection as $user) {
            if ($user->username === $newUsername) {
                if ($requestedUserUuid !== null && $requestedUserUuid === $user->uuid) {
                    return $response;
                }
                $response->setSuccess(false);
                $msg = __('hybridcms::admin.save.user.name.exists.error', ['name' => $newUsername]);
                $response->setMessage($msg);
                return $response;
            }
        }
        return $response;
    }

    /**
     * @param mixed $password 
     * @param mixed $cpassword 
     * @param ResponseTransfer $response 
     * @return ResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function validatePassword(
        $password,
        $cpassword,
        ResponseTransfer $response
    ): ResponseTransfer {

        if ($response->getSuccess() === false) {
            return $response;
        }

        $passwordValidationHelper = new PasswordValidationHelper;
        $pwRequirements = DB::table('settings_password')
            ->select(['settings_password.min_chars', 'settings_password.number_required', 'settings_password.capital_required'])
            ->first();

        if ($password || $cpassword !== null) {
            $passwordValidationHelper->passwordExists($password, $response);
            $passwordValidationHelper->passwordExists($cpassword, $response);

            if ($response->getSuccess() !== true) {
                return $response;
            }

            $passwordValidationHelper->passwodHasMinLength($password, $pwRequirements->min_chars, $response);
            $passwordValidationHelper->passwodHasMinLength($cpassword, $pwRequirements->min_chars, $response);

            if ($response->getSuccess() !== true) {
                return $response;
            }

            if ($pwRequirements->capital_required === 1) {
                $passwordValidationHelper->passwordHasCapitalLetter($password, $response);
                $passwordValidationHelper->passwordHasCapitalLetter($cpassword, $response);

                if ($response->getSuccess() !== true) {
                    return $response;
                }
            }

            if ($pwRequirements->number_required === 1) {
                $passwordValidationHelper->passwordHasNumber($password, $response);
                $passwordValidationHelper->passwordHasNumber($cpassword, $response);

                if ($response->getSuccess() !== true) {
                    return $response;
                }
            }

            if ($password !== $cpassword) {
                $response->setSuccess(false);
                $msg = __('hybridcms::admin.save.user.pw.not.identic.error');
                $response->setMessage($msg);
                return $response;
            }
        }

        return $response;
    }

    /**
     * @param Request $request 
     * @param ResponseTransfer $response 
     * @return ResponseTransfer 
     * @throws InvalidArgumentException 
     * @throws BindingResolutionException 
     */
    public function validateRoleSelection(
        Request $request,
        ResponseTransfer $response,
        string $currentUserUuid
    ): ResponseTransfer {

        if ($response->getSuccess() === false) {
            return $response;
        }

        $requestedUserUuid = $request->input('user_uuid');

        $roles = $request->input("roles");

        if (empty($roles)) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.missing.role.error');
            $response->setMessage($msg);
            return $response;
        }

        // check if the current user wants to delete his admin rights
        if ($requestedUserUuid !== null) {
            if ($currentUserUuid === $requestedUserUuid) {
                $adminRoleId = DB::table('roles')
                    ->where('roles.role', '=', 'admin')
                    ->select(['roles.id'])
                    ->first();

                $exists = in_array($adminRoleId->id, $roles);

                if ($exists == false) {
                    $response->setSuccess(false);
                    $msg = __('hybridcms::admin.save.user.admin.role.error');
                    $response->setMessage($msg);
                }
            }
        }

        return $response;
    }
}
