<?php

namespace Leonp5\Hybridcms\Helper\PasswordValidation;

use Leonp5\Hybridcms\Transfer\ResponseTransferInterface;

class PasswordValidationHelper implements
    PasswordValidationHelperInterface
{

    public function passwordExists(
        $password,
        ResponseTransferInterface $response
    ): ResponseTransferInterface {
        if ($password === null) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.pw.empty.error');
            $response->setMessage($msg);
        }
        return $response;
    }

    public function passwodHasMinLength(
        string $password,
        int $minLength,
        ResponseTransferInterface $response
    ): ResponseTransferInterface {
        if (strlen($password) < $minLength) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.short.pw.error', ['minLength' => $minLength]);
            $response->setMessage($msg);
        }
        return $response;
    }

    public function passwordHasCapitalLetter(
        string $password,
        ResponseTransferInterface $response
    ): ResponseTransferInterface {
        if (preg_match('/[A-Z]/', $password) == false) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.capital.pw.error');
            $response->setMessage($msg);
        }
        return $response;
    }

    public function passwordHasNumber(
        string $password,
        ResponseTransferInterface $response
    ): ResponseTransferInterface {
        if (preg_match('/[0-9]/', $password) == false) {
            $response->setSuccess(false);
            $msg = __('hybridcms::admin.save.user.number.pw.error');
            $response->setMessage($msg);
        }
        return $response;
    }
}
