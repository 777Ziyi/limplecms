<?php

namespace Leonp5\Hybridcms\Helper\PasswordValidation;

use Leonp5\Hybridcms\Transfer\ResponseTransferInterface;

interface PasswordValidationHelperInterface
{
    /**
     * @param mixed $password 
     * @param ResponseTransferInterface $response 
     * @return ResponseTransferInterface 
     */
    public function passwordExists(
        $password,
        ResponseTransferInterface $response,
    ): ResponseTransferInterface;

    /**
     * @param string $password 
     * @param int $minLength 
     * @param ResponseTransferInterface $response 
     * @return ResponseTransferInterface 
     */
    public function passwodHasMinLength(
        string $password,
        int $minLength,
        ResponseTransferInterface $response
    ): ResponseTransferInterface;

    /**
     * @param string $password 
     * @param ResponseTransferInterface $response 
     * @return ResponseTransferInterface 
     */
    public function passwordHasCapitalLetter(
        string $password,
        ResponseTransferInterface $response
    ): ResponseTransferInterface;

    /**
     * @param string $password 
     * @param ResponseTransferInterface $response 
     * @return ResponseTransferInterface 
     */
    public function passwordHasNumber(
        string $password,
        ResponseTransferInterface $response
    ): ResponseTransferInterface;
}
