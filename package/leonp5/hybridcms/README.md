# HybridCMS

A simple Laravel CMS package. Intended for private usage.

### Customizing

---

php artisan vendor:publish --provider="Leonp5\Hybridcms\HybridCmsProvider" --tag="assets"
