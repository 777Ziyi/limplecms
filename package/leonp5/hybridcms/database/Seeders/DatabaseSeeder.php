<?php

namespace Leonp5\Hybridcms\Database\Seeders;

use Illuminate\Database\Seeder;

use Leonp5\Hybridcms\Database\Seeders\Seeds\SettingsSeeder;
use Leonp5\Hybridcms\Database\Seeders\Seeds\LanguagesSeeder;
use Leonp5\Hybridcms\Database\Seeders\Seeds\RolesTableSeeder;
use Leonp5\Hybridcms\Database\Seeders\Seeds\PageStatusSeeder;
use Leonp5\Hybridcms\Database\Seeders\Seeds\UsersTableSeeder;
use Leonp5\Hybridcms\Database\Seeders\Seeds\PagesContentTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // order is important here
        $this->call([
            RolesTableSeeder::class,
            PageStatusSeeder::class,
            UsersTableSeeder::class,
            PagesContentTableSeeder::class,
            LanguagesSeeder::class,
            SettingsSeeder::class
        ]);
    }
}
