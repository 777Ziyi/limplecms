<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use Leonp5\Hybridcms\Models\Role;
use Leonp5\Hybridcms\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $adminRole = Role::where('role', 'admin')->first();
        $authorRole = Role::where('role', 'author')->first();
        $editorRole = Role::where('role', 'editor')->first();

        User::truncate();

        $admin = User::create([
            'uuid' => Str::uuid(),
            'username' => 'admin',
            'email' => 'test@test.com',
            'password' => Hash::make('1234')
        ]);

        $author = User::create([
            'uuid' => Str::uuid(),
            'username' => 'author',
            'email' => 'test1@test.com',
            'password' => Hash::make('1234')
        ]);

        $editor = User::create([
            'uuid' => Str::uuid(),
            'username' => 'editor',
            'email' => 'test2@test.com',
            'password' => Hash::make('1234')
        ]);

        $admin->roles()->attach($adminRole);
        $author->roles()->attach($authorRole);
        $editor->roles()->attach($editorRole);
    }
}
