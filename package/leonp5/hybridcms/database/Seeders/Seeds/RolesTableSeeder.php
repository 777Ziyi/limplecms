<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;

use Leonp5\Hybridcms\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * 
     * @return void 
     */
    public function run(): void
    {
        Role::truncate();

        Role::create(['role' => 'admin']);
        Role::create(['role' => 'editor']);
        Role::create(['role' => 'author']);
    }
}
