<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Models\ActivatedLanguages;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language_de')->insert(LanguageDE::getLanguageArray());
        DB::table('language_gb')->insert(LanguageGB::getLanguageArray());

        ActivatedLanguages::create([
            'alpha_2' => 'de',
            'lang_id' => 276,
            'main_language' => 1
        ]);

        ActivatedLanguages::create([
            'alpha_2' => 'gb',
            'lang_id' => 826
        ]);
    }
}
