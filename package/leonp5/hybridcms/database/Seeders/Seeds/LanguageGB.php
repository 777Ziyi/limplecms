<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;

class LanguageGB extends Seeder
{
    public static function getLanguageArray()
    {
        return  array(
            array(
                'id'        => 4,
                'name'      => 'Afghanistan',
                'alpha_2'    => 'af',
                'alpha_3'    => 'afg'
            ),
            array(
                'id'        => 248,
                'name'      => 'Åland Islands',
                'alpha_2'    => 'ax',
                'alpha_3'    => 'ala'
            ),
            array(
                'id'        => 8,
                'name'      => 'Albania',
                'alpha_2'    => 'al',
                'alpha_3'    => 'alb'
            ),
            array(
                'id'        => 12,
                'name'      => 'Algeria',
                'alpha_2'    => 'dz',
                'alpha_3'    => 'dza'
            ),
            array(
                'id'        => 16,
                'name'      => 'American Samoa',
                'alpha_2'    => 'as',
                'alpha_3'    => 'asm'
            ),
            array(
                'id'        => 20,
                'name'      => 'Andorra',
                'alpha_2'    => 'ad',
                'alpha_3'    => 'and'
            ),
            array(
                'id'        => 24,
                'name'      => 'Angola',
                'alpha_2'    => 'ao',
                'alpha_3'    => 'ago'
            ),
            array(
                'id'        => 660,
                'name'      => 'Anguilla',
                'alpha_2'    => 'ai',
                'alpha_3'    => 'aia'
            ),
            array(
                'id'        => 10,
                'name'      => 'Antarctica',
                'alpha_2'    => 'aq',
                'alpha_3'    => 'ata'
            ),
            array(
                'id'        => 28,
                'name'      => 'Antigua and Barbuda',
                'alpha_2'    => 'ag',
                'alpha_3'    => 'atg'
            ),
            array(
                'id'        => 32,
                'name'      => 'Argentina',
                'alpha_2'    => 'ar',
                'alpha_3'    => 'arg'
            ),
            array(
                'id'        => 51,
                'name'      => 'Armenia',
                'alpha_2'    => 'am',
                'alpha_3'    => 'arm'
            ),
            array(
                'id'        => 533,
                'name'      => 'Aruba',
                'alpha_2'    => 'aw',
                'alpha_3'    => 'abw'
            ),
            array(
                'id'        => 36,
                'name'      => 'Australia',
                'alpha_2'    => 'au',
                'alpha_3'    => 'aus'
            ),
            array(
                'id'        => 40,
                'name'      => 'Austria',
                'alpha_2'    => 'at',
                'alpha_3'    => 'aut'
            ),
            array(
                'id'        => 31,
                'name'      => 'Azerbaijan',
                'alpha_2'    => 'az',
                'alpha_3'    => 'aze'
            ),
            array(
                'id'        => 44,
                'name'      => 'Bahamas',
                'alpha_2'    => 'bs',
                'alpha_3'    => 'bhs'
            ),
            array(
                'id'        => 48,
                'name'      => 'Bahrain',
                'alpha_2'    => 'bh',
                'alpha_3'    => 'bhr'
            ),
            array(
                'id'        => 50,
                'name'      => 'Bangladesh',
                'alpha_2'    => 'bd',
                'alpha_3'    => 'bgd'
            ),
            array(
                'id'        => 52,
                'name'      => 'Barbados',
                'alpha_2'    => 'bb',
                'alpha_3'    => 'brb'
            ),
            array(
                'id'        => 112,
                'name'      => 'Belarus',
                'alpha_2'    => 'by',
                'alpha_3'    => 'blr'
            ),
            array(
                'id'        => 56,
                'name'      => 'Belgium',
                'alpha_2'    => 'be',
                'alpha_3'    => 'bel'
            ),
            array(
                'id'        => 84,
                'name'      => 'Belize',
                'alpha_2'    => 'bz',
                'alpha_3'    => 'blz'
            ),
            array(
                'id'        => 204,
                'name'      => 'Benin',
                'alpha_2'    => 'bj',
                'alpha_3'    => 'ben'
            ),
            array(
                'id'        => 60,
                'name'      => 'Bermuda',
                'alpha_2'    => 'bm',
                'alpha_3'    => 'bmu'
            ),
            array(
                'id'        => 64,
                'name'      => 'Bhutan',
                'alpha_2'    => 'bt',
                'alpha_3'    => 'btn'
            ),
            array(
                'id'        => 68,
                'name'      => 'Bolivia (Plurinational State of)',
                'alpha_2'    => 'bo',
                'alpha_3'    => 'bol'
            ),
            array(
                'id'        => 535,
                'name'      => 'Bonaire, Sint Eustatius and Saba',
                'alpha_2'    => 'bq',
                'alpha_3'    => 'bes'
            ),
            array(
                'id'        => 70,
                'name'      => 'Bosnia and Herzegovina',
                'alpha_2'    => 'ba',
                'alpha_3'    => 'bih'
            ),
            array(
                'id'        => 72,
                'name'      => 'Botswana',
                'alpha_2'    => 'bw',
                'alpha_3'    => 'bwa'
            ),
            array(
                'id'        => 74,
                'name'      => 'Bouvet Island',
                'alpha_2'    => 'bv',
                'alpha_3'    => 'bvt'
            ),
            array(
                'id'        => 76,
                'name'      => 'Brazil',
                'alpha_2'    => 'br',
                'alpha_3'    => 'bra'
            ),
            array(
                'id'        => 86,
                'name'      => 'British Indian Ocean Territory',
                'alpha_2'    => 'io',
                'alpha_3'    => 'iot'
            ),
            array(
                'id'        => 96,
                'name'      => 'Brunei Darussalam',
                'alpha_2'    => 'bn',
                'alpha_3'    => 'brn'
            ),
            array(
                'id'        => 100,
                'name'      => 'Bulgaria',
                'alpha_2'    => 'bg',
                'alpha_3'    => 'bgr'
            ),
            array(
                'id'        => 854,
                'name'      => 'Burkina Faso',
                'alpha_2'    => 'bf',
                'alpha_3'    => 'bfa'
            ),
            array(
                'id'        => 108,
                'name'      => 'Burundi',
                'alpha_2'    => 'bi',
                'alpha_3'    => 'bdi'
            ),
            array(
                'id'        => 132,
                'name'      => 'Cabo Verde',
                'alpha_2'    => 'cv',
                'alpha_3'    => 'cpv'
            ),
            array(
                'id'        => 116,
                'name'      => 'Cambodia',
                'alpha_2'    => 'kh',
                'alpha_3'    => 'khm'
            ),
            array(
                'id'        => 120,
                'name'      => 'Cameroon',
                'alpha_2'    => 'cm',
                'alpha_3'    => 'cmr'
            ),
            array(
                'id'        => 124,
                'name'      => 'Canada',
                'alpha_2'    => 'ca',
                'alpha_3'    => 'can'
            ),
            array(
                'id'        => 136,
                'name'      => 'Cayman Islands',
                'alpha_2'    => 'ky',
                'alpha_3'    => 'cym'
            ),
            array(
                'id'        => 140,
                'name'      => 'Central African Republic',
                'alpha_2'    => 'cf',
                'alpha_3'    => 'caf'
            ),
            array(
                'id'        => 148,
                'name'      => 'Chad',
                'alpha_2'    => 'td',
                'alpha_3'    => 'tcd'
            ),
            array(
                'id'        => 152,
                'name'      => 'Chile',
                'alpha_2'    => 'cl',
                'alpha_3'    => 'chl'
            ),
            array(
                'id'        => 156,
                'name'      => 'China',
                'alpha_2'    => 'cn',
                'alpha_3'    => 'chn'
            ),
            array(
                'id'        => 162,
                'name'      => 'Christmas Island',
                'alpha_2'    => 'cx',
                'alpha_3'    => 'cxr'
            ),
            array(
                'id'        => 166,
                'name'      => 'Cocos (Keeling) Islands',
                'alpha_2'    => 'cc',
                'alpha_3'    => 'cck'
            ),
            array(
                'id'        => 170,
                'name'      => 'Colombia',
                'alpha_2'    => 'co',
                'alpha_3'    => 'col'
            ),
            array(
                'id'        => 174,
                'name'      => 'Comoros',
                'alpha_2'    => 'km',
                'alpha_3'    => 'com'
            ),
            array(
                'id'        => 178,
                'name'      => 'Congo',
                'alpha_2'    => 'cg',
                'alpha_3'    => 'cog'
            ),
            array(
                'id'        => 180,
                'name'      => 'Congo, Democratic Republic of the',
                'alpha_2'    => 'cd',
                'alpha_3'    => 'cod'
            ),
            array(
                'id'        => 184,
                'name'      => 'Cook Islands',
                'alpha_2'    => 'ck',
                'alpha_3'    => 'cok'
            ),
            array(
                'id'        => 188,
                'name'      => 'Costa Rica',
                'alpha_2'    => 'cr',
                'alpha_3'    => 'cri'
            ),
            array(
                'id'        => 384,
                'name'      => 'Côte d\'Ivoire',
                'alpha_2'    => 'ci',
                'alpha_3'    => 'civ'
            ),
            array(
                'id'        => 191,
                'name'      => 'Croatia',
                'alpha_2'    => 'hr',
                'alpha_3'    => 'hrv'
            ),
            array(
                'id'        => 192,
                'name'      => 'Cuba',
                'alpha_2'    => 'cu',
                'alpha_3'    => 'cub'
            ),
            array(
                'id'        => 531,
                'name'      => 'Curaçao',
                'alpha_2'    => 'cw',
                'alpha_3'    => 'cuw'
            ),
            array(
                'id'        => 196,
                'name'      => 'Cyprus',
                'alpha_2'    => 'cy',
                'alpha_3'    => 'cyp'
            ),
            array(
                'id'        => 203,
                'name'      => 'Czechia',
                'alpha_2'    => 'cz',
                'alpha_3'    => 'cze'
            ),
            array(
                'id'        => 208,
                'name'      => 'Denmark',
                'alpha_2'    => 'dk',
                'alpha_3'    => 'dnk'
            ),
            array(
                'id'        => 262,
                'name'      => 'Djibouti',
                'alpha_2'    => 'dj',
                'alpha_3'    => 'dji'
            ),
            array(
                'id'        => 212,
                'name'      => 'Dominica',
                'alpha_2'    => 'dm',
                'alpha_3'    => 'dma'
            ),
            array(
                'id'        => 214,
                'name'      => 'Dominican Republic',
                'alpha_2'    => 'do',
                'alpha_3'    => 'dom'
            ),
            array(
                'id'        => 218,
                'name'      => 'Ecuador',
                'alpha_2'    => 'ec',
                'alpha_3'    => 'ecu'
            ),
            array(
                'id'        => 818,
                'name'      => 'Egypt',
                'alpha_2'    => 'eg',
                'alpha_3'    => 'egy'
            ),
            array(
                'id'        => 222,
                'name'      => 'El Salvador',
                'alpha_2'    => 'sv',
                'alpha_3'    => 'slv'
            ),
            array(
                'id'        => 226,
                'name'      => 'Equatorial Guinea',
                'alpha_2'    => 'gq',
                'alpha_3'    => 'gnq'
            ),
            array(
                'id'        => 232,
                'name'      => 'Eritrea',
                'alpha_2'    => 'er',
                'alpha_3'    => 'eri'
            ),
            array(
                'id'        => 233,
                'name'      => 'Estonia',
                'alpha_2'    => 'ee',
                'alpha_3'    => 'est'
            ),
            array(
                'id'        => 748,
                'name'      => 'Eswatini',
                'alpha_2'    => 'sz',
                'alpha_3'    => 'swz'
            ),
            array(
                'id'        => 231,
                'name'      => 'Ethiopia',
                'alpha_2'    => 'et',
                'alpha_3'    => 'eth'
            ),
            array(
                'id'        => 238,
                'name'      => 'Falkland Islands (Malvinas)',
                'alpha_2'    => 'fk',
                'alpha_3'    => 'flk'
            ),
            array(
                'id'        => 234,
                'name'      => 'Faroe Islands',
                'alpha_2'    => 'fo',
                'alpha_3'    => 'fro'
            ),
            array(
                'id'        => 242,
                'name'      => 'Fiji',
                'alpha_2'    => 'fj',
                'alpha_3'    => 'fji'
            ),
            array(
                'id'        => 246,
                'name'      => 'Finland',
                'alpha_2'    => 'fi',
                'alpha_3'    => 'fin'
            ),
            array(
                'id'        => 250,
                'name'      => 'France',
                'alpha_2'    => 'fr',
                'alpha_3'    => 'fra'
            ),
            array(
                'id'        => 254,
                'name'      => 'French Guiana',
                'alpha_2'    => 'gf',
                'alpha_3'    => 'guf'
            ),
            array(
                'id'        => 258,
                'name'      => 'French Polynesia',
                'alpha_2'    => 'pf',
                'alpha_3'    => 'pyf'
            ),
            array(
                'id'        => 260,
                'name'      => 'French Southern Territories',
                'alpha_2'    => 'tf',
                'alpha_3'    => 'atf'
            ),
            array(
                'id'        => 266,
                'name'      => 'Gabon',
                'alpha_2'    => 'ga',
                'alpha_3'    => 'gab'
            ),
            array(
                'id'        => 270,
                'name'      => 'Gambia',
                'alpha_2'    => 'gm',
                'alpha_3'    => 'gmb'
            ),
            array(
                'id'        => 268,
                'name'      => 'Georgia',
                'alpha_2'    => 'ge',
                'alpha_3'    => 'geo'
            ),
            array(
                'id'        => 276,
                'name'      => 'Germany',
                'alpha_2'    => 'de',
                'alpha_3'    => 'deu'
            ),
            array(
                'id'        => 288,
                'name'      => 'Ghana',
                'alpha_2'    => 'gh',
                'alpha_3'    => 'gha'
            ),
            array(
                'id'        => 292,
                'name'      => 'Gibraltar',
                'alpha_2'    => 'gi',
                'alpha_3'    => 'gib'
            ),
            array(
                'id'        => 300,
                'name'      => 'Greece',
                'alpha_2'    => 'gr',
                'alpha_3'    => 'grc'
            ),
            array(
                'id'        => 304,
                'name'      => 'Greenland',
                'alpha_2'    => 'gl',
                'alpha_3'    => 'grl'
            ),
            array(
                'id'        => 308,
                'name'      => 'Grenada',
                'alpha_2'    => 'gd',
                'alpha_3'    => 'grd'
            ),
            array(
                'id'        => 312,
                'name'      => 'Guadeloupe',
                'alpha_2'    => 'gp',
                'alpha_3'    => 'glp'
            ),
            array(
                'id'        => 316,
                'name'      => 'Guam',
                'alpha_2'    => 'gu',
                'alpha_3'    => 'gum'
            ),
            array(
                'id'        => 320,
                'name'      => 'Guatemala',
                'alpha_2'    => 'gt',
                'alpha_3'    => 'gtm'
            ),
            array(
                'id'        => 831,
                'name'      => 'Guernsey',
                'alpha_2'    => 'gg',
                'alpha_3'    => 'ggy'
            ),
            array(
                'id'        => 324,
                'name'      => 'Guinea',
                'alpha_2'    => 'gn',
                'alpha_3'    => 'gin'
            ),
            array(
                'id'        => 624,
                'name'      => 'Guinea-Bissau',
                'alpha_2'    => 'gw',
                'alpha_3'    => 'gnb'
            ),
            array(
                'id'        => 328,
                'name'      => 'Guyana',
                'alpha_2'    => 'gy',
                'alpha_3'    => 'guy'
            ),
            array(
                'id'        => 332,
                'name'      => 'Haiti',
                'alpha_2'    => 'ht',
                'alpha_3'    => 'hti'
            ),
            array(
                'id'        => 334,
                'name'      => 'Heard Island and McDonald Islands',
                'alpha_2'    => 'hm',
                'alpha_3'    => 'hmd'
            ),
            array(
                'id'        => 336,
                'name'      => 'Holy See',
                'alpha_2'    => 'va',
                'alpha_3'    => 'vat'
            ),
            array(
                'id'        => 340,
                'name'      => 'Honduras',
                'alpha_2'    => 'hn',
                'alpha_3'    => 'hnd'
            ),
            array(
                'id'        => 344,
                'name'      => 'Hong Kong',
                'alpha_2'    => 'hk',
                'alpha_3'    => 'hkg'
            ),
            array(
                'id'        => 348,
                'name'      => 'Hungary',
                'alpha_2'    => 'hu',
                'alpha_3'    => 'hun'
            ),
            array(
                'id'        => 352,
                'name'      => 'Iceland',
                'alpha_2'    => 'is',
                'alpha_3'    => 'isl'
            ),
            array(
                'id'        => 356,
                'name'      => 'India',
                'alpha_2'    => 'in',
                'alpha_3'    => 'ind'
            ),
            array(
                'id'        => 360,
                'name'      => 'Indonesia',
                'alpha_2'    => 'id',
                'alpha_3'    => 'idn'
            ),
            array(
                'id'        => 364,
                'name'      => 'Iran (Islamic Republic of)',
                'alpha_2'    => 'ir',
                'alpha_3'    => 'irn'
            ),
            array(
                'id'        => 368,
                'name'      => 'Iraq',
                'alpha_2'    => 'iq',
                'alpha_3'    => 'irq'
            ),
            array(
                'id'        => 372,
                'name'      => 'Ireland',
                'alpha_2'    => 'ie',
                'alpha_3'    => 'irl'
            ),
            array(
                'id'        => 833,
                'name'      => 'Isle of Man',
                'alpha_2'    => 'im',
                'alpha_3'    => 'imn'
            ),
            array(
                'id'        => 376,
                'name'      => 'Israel',
                'alpha_2'    => 'il',
                'alpha_3'    => 'isr'
            ),
            array(
                'id'        => 380,
                'name'      => 'Italy',
                'alpha_2'    => 'it',
                'alpha_3'    => 'ita'
            ),
            array(
                'id'        => 388,
                'name'      => 'Jamaica',
                'alpha_2'    => 'jm',
                'alpha_3'    => 'jam'
            ),
            array(
                'id'        => 392,
                'name'      => 'Japan',
                'alpha_2'    => 'jp',
                'alpha_3'    => 'jpn'
            ),
            array(
                'id'        => 832,
                'name'      => 'Jersey',
                'alpha_2'    => 'je',
                'alpha_3'    => 'jey'
            ),
            array(
                'id'        => 400,
                'name'      => 'Jordan',
                'alpha_2'    => 'jo',
                'alpha_3'    => 'jor'
            ),
            array(
                'id'        => 398,
                'name'      => 'Kazakhstan',
                'alpha_2'    => 'kz',
                'alpha_3'    => 'kaz'
            ),
            array(
                'id'        => 404,
                'name'      => 'Kenya',
                'alpha_2'    => 'ke',
                'alpha_3'    => 'ken'
            ),
            array(
                'id'        => 296,
                'name'      => 'Kiribati',
                'alpha_2'    => 'ki',
                'alpha_3'    => 'kir'
            ),
            array(
                'id'        => 408,
                'name'      => 'Korea (Democratic People\'s Republic of)',
                'alpha_2'    => 'kp',
                'alpha_3'    => 'prk'
            ),
            array(
                'id'        => 410,
                'name'      => 'Korea, Republic of',
                'alpha_2'    => 'kr',
                'alpha_3'    => 'kor'
            ),
            array(
                'id'        => 414,
                'name'      => 'Kuwait',
                'alpha_2'    => 'kw',
                'alpha_3'    => 'kwt'
            ),
            array(
                'id'        => 417,
                'name'      => 'Kyrgyzstan',
                'alpha_2'    => 'kg',
                'alpha_3'    => 'kgz'
            ),
            array(
                'id'        => 418,
                'name'      => 'Lao People\'s Democratic Republic',
                'alpha_2'    => 'la',
                'alpha_3'    => 'lao'
            ),
            array(
                'id'        => 428,
                'name'      => 'Latvia',
                'alpha_2'    => 'lv',
                'alpha_3'    => 'lva'
            ),
            array(
                'id'        => 422,
                'name'      => 'Lebanon',
                'alpha_2'    => 'lb',
                'alpha_3'    => 'lbn'
            ),
            array(
                'id'        => 426,
                'name'      => 'Lesotho',
                'alpha_2'    => 'ls',
                'alpha_3'    => 'lso'
            ),
            array(
                'id'        => 430,
                'name'      => 'Liberia',
                'alpha_2'    => 'lr',
                'alpha_3'    => 'lbr'
            ),
            array(
                'id'        => 434,
                'name'      => 'Libya',
                'alpha_2'    => 'ly',
                'alpha_3'    => 'lby'
            ),
            array(
                'id'        => 438,
                'name'      => 'Liechtenstein',
                'alpha_2'    => 'li',
                'alpha_3'    => 'lie'
            ),
            array(
                'id'        => 440,
                'name'      => 'Lithuania',
                'alpha_2'    => 'lt',
                'alpha_3'    => 'ltu'
            ),
            array(
                'id'        => 442,
                'name'      => 'Luxembourg',
                'alpha_2'    => 'lu',
                'alpha_3'    => 'lux'
            ),
            array(
                'id'        => 446,
                'name'      => 'Macao',
                'alpha_2'    => 'mo',
                'alpha_3'    => 'mac'
            ),
            array(
                'id'        => 450,
                'name'      => 'Madagascar',
                'alpha_2'    => 'mg',
                'alpha_3'    => 'mdg'
            ),
            array(
                'id'        => 454,
                'name'      => 'Malawi',
                'alpha_2'    => 'mw',
                'alpha_3'    => 'mwi'
            ),
            array(
                'id'        => 458,
                'name'      => 'Malaysia',
                'alpha_2'    => 'my',
                'alpha_3'    => 'mys'
            ),
            array(
                'id'        => 462,
                'name'      => 'Maldives',
                'alpha_2'    => 'mv',
                'alpha_3'    => 'mdv'
            ),
            array(
                'id'        => 466,
                'name'      => 'Mali',
                'alpha_2'    => 'ml',
                'alpha_3'    => 'mli'
            ),
            array(
                'id'        => 470,
                'name'      => 'Malta',
                'alpha_2'    => 'mt',
                'alpha_3'    => 'mlt'
            ),
            array(
                'id'        => 584,
                'name'      => 'Marshall Islands',
                'alpha_2'    => 'mh',
                'alpha_3'    => 'mhl'
            ),
            array(
                'id'        => 474,
                'name'      => 'Martinique',
                'alpha_2'    => 'mq',
                'alpha_3'    => 'mtq'
            ),
            array(
                'id'        => 478,
                'name'      => 'Mauritania',
                'alpha_2'    => 'mr',
                'alpha_3'    => 'mrt'
            ),
            array(
                'id'        => 480,
                'name'      => 'Mauritius',
                'alpha_2'    => 'mu',
                'alpha_3'    => 'mus'
            ),
            array(
                'id'        => 175,
                'name'      => 'Mayotte',
                'alpha_2'    => 'yt',
                'alpha_3'    => 'myt'
            ),
            array(
                'id'        => 484,
                'name'      => 'Mexico',
                'alpha_2'    => 'mx',
                'alpha_3'    => 'mex'
            ),
            array(
                'id'        => 583,
                'name'      => 'Micronesia (Federated States of)',
                'alpha_2'    => 'fm',
                'alpha_3'    => 'fsm'
            ),
            array(
                'id'        => 498,
                'name'      => 'Moldova, Republic of',
                'alpha_2'    => 'md',
                'alpha_3'    => 'mda'
            ),
            array(
                'id'        => 492,
                'name'      => 'Monaco',
                'alpha_2'    => 'mc',
                'alpha_3'    => 'mco'
            ),
            array(
                'id'        => 496,
                'name'      => 'Mongolia',
                'alpha_2'    => 'mn',
                'alpha_3'    => 'mng'
            ),
            array(
                'id'        => 499,
                'name'      => 'Montenegro',
                'alpha_2'    => 'me',
                'alpha_3'    => 'mne'
            ),
            array(
                'id'        => 500,
                'name'      => 'Montserrat',
                'alpha_2'    => 'ms',
                'alpha_3'    => 'msr'
            ),
            array(
                'id'        => 504,
                'name'      => 'Morocco',
                'alpha_2'    => 'ma',
                'alpha_3'    => 'mar'
            ),
            array(
                'id'        => 508,
                'name'      => 'Mozambique',
                'alpha_2'    => 'mz',
                'alpha_3'    => 'moz'
            ),
            array(
                'id'        => 104,
                'name'      => 'Myanmar',
                'alpha_2'    => 'mm',
                'alpha_3'    => 'mmr'
            ),
            array(
                'id'        => 516,
                'name'      => 'Namibia',
                'alpha_2'    => 'na',
                'alpha_3'    => 'nam'
            ),
            array(
                'id'        => 520,
                'name'      => 'Nauru',
                'alpha_2'    => 'nr',
                'alpha_3'    => 'nru'
            ),
            array(
                'id'        => 524,
                'name'      => 'Nepal',
                'alpha_2'    => 'np',
                'alpha_3'    => 'npl'
            ),
            array(
                'id'        => 528,
                'name'      => 'Netherlands',
                'alpha_2'    => 'nl',
                'alpha_3'    => 'nld'
            ),
            array(
                'id'        => 540,
                'name'      => 'New Caledonia',
                'alpha_2'    => 'nc',
                'alpha_3'    => 'ncl'
            ),
            array(
                'id'        => 554,
                'name'      => 'New Zealand',
                'alpha_2'    => 'nz',
                'alpha_3'    => 'nzl'
            ),
            array(
                'id'        => 558,
                'name'      => 'Nicaragua',
                'alpha_2'    => 'ni',
                'alpha_3'    => 'nic'
            ),
            array(
                'id'        => 562,
                'name'      => 'Niger',
                'alpha_2'    => 'ne',
                'alpha_3'    => 'ner'
            ),
            array(
                'id'        => 566,
                'name'      => 'Nigeria',
                'alpha_2'    => 'ng',
                'alpha_3'    => 'nga'
            ),
            array(
                'id'        => 570,
                'name'      => 'Niue',
                'alpha_2'    => 'nu',
                'alpha_3'    => 'niu'
            ),
            array(
                'id'        => 574,
                'name'      => 'Norfolk Island',
                'alpha_2'    => 'nf',
                'alpha_3'    => 'nfk'
            ),
            array(
                'id'        => 807,
                'name'      => 'North Macedonia',
                'alpha_2'    => 'mk',
                'alpha_3'    => 'mkd'
            ),
            array(
                'id'        => 580,
                'name'      => 'Northern Mariana Islands',
                'alpha_2'    => 'mp',
                'alpha_3'    => 'mnp'
            ),
            array(
                'id'        => 578,
                'name'      => 'Norway',
                'alpha_2'    => 'no',
                'alpha_3'    => 'nor'
            ),
            array(
                'id'        => 512,
                'name'      => 'Oman',
                'alpha_2'    => 'om',
                'alpha_3'    => 'omn'
            ),
            array(
                'id'        => 586,
                'name'      => 'Pakistan',
                'alpha_2'    => 'pk',
                'alpha_3'    => 'pak'
            ),
            array(
                'id'        => 585,
                'name'      => 'Palau',
                'alpha_2'    => 'pw',
                'alpha_3'    => 'plw'
            ),
            array(
                'id'        => 275,
                'name'      => 'Palestine, State of',
                'alpha_2'    => 'ps',
                'alpha_3'    => 'pse'
            ),
            array(
                'id'        => 591,
                'name'      => 'Panama',
                'alpha_2'    => 'pa',
                'alpha_3'    => 'pan'
            ),
            array(
                'id'        => 598,
                'name'      => 'Papua New Guinea',
                'alpha_2'    => 'pg',
                'alpha_3'    => 'png'
            ),
            array(
                'id'        => 600,
                'name'      => 'Paraguay',
                'alpha_2'    => 'py',
                'alpha_3'    => 'pry'
            ),
            array(
                'id'        => 604,
                'name'      => 'Peru',
                'alpha_2'    => 'pe',
                'alpha_3'    => 'per'
            ),
            array(
                'id'        => 608,
                'name'      => 'Philippines',
                'alpha_2'    => 'ph',
                'alpha_3'    => 'phl'
            ),
            array(
                'id'        => 612,
                'name'      => 'Pitcairn',
                'alpha_2'    => 'pn',
                'alpha_3'    => 'pcn'
            ),
            array(
                'id'        => 616,
                'name'      => 'Poland',
                'alpha_2'    => 'pl',
                'alpha_3'    => 'pol'
            ),
            array(
                'id'        => 620,
                'name'      => 'Portugal',
                'alpha_2'    => 'pt',
                'alpha_3'    => 'prt'
            ),
            array(
                'id'        => 630,
                'name'      => 'Puerto Rico',
                'alpha_2'    => 'pr',
                'alpha_3'    => 'pri'
            ),
            array(
                'id'        => 634,
                'name'      => 'Qatar',
                'alpha_2'    => 'qa',
                'alpha_3'    => 'qat'
            ),
            array(
                'id'        => 638,
                'name'      => 'Réunion',
                'alpha_2'    => 're',
                'alpha_3'    => 'reu'
            ),
            array(
                'id'        => 642,
                'name'      => 'Romania',
                'alpha_2'    => 'ro',
                'alpha_3'    => 'rou'
            ),
            array(
                'id'        => 643,
                'name'      => 'Russian Federation',
                'alpha_2'    => 'ru',
                'alpha_3'    => 'rus'
            ),
            array(
                'id'        => 646,
                'name'      => 'Rwanda',
                'alpha_2'    => 'rw',
                'alpha_3'    => 'rwa'
            ),
            array(
                'id'        => 652,
                'name'      => 'Saint Barthélemy',
                'alpha_2'    => 'bl',
                'alpha_3'    => 'blm'
            ),
            array(
                'id'        => 654,
                'name'      => 'Saint Helena, Ascension and Tristan da Cunha',
                'alpha_2'    => 'sh',
                'alpha_3'    => 'shn'
            ),
            array(
                'id'        => 659,
                'name'      => 'Saint Kitts and Nevis',
                'alpha_2'    => 'kn',
                'alpha_3'    => 'kna'
            ),
            array(
                'id'        => 662,
                'name'      => 'Saint Lucia',
                'alpha_2'    => 'lc',
                'alpha_3'    => 'lca'
            ),
            array(
                'id'        => 663,
                'name'      => 'Saint Martin (French part)',
                'alpha_2'    => 'mf',
                'alpha_3'    => 'maf'
            ),
            array(
                'id'        => 666,
                'name'      => 'Saint Pierre and Miquelon',
                'alpha_2'    => 'pm',
                'alpha_3'    => 'spm'
            ),
            array(
                'id'        => 670,
                'name'      => 'Saint Vincent and the Grenadines',
                'alpha_2'    => 'vc',
                'alpha_3'    => 'vct'
            ),
            array(
                'id'        => 882,
                'name'      => 'Samoa',
                'alpha_2'    => 'ws',
                'alpha_3'    => 'wsm'
            ),
            array(
                'id'        => 674,
                'name'      => 'San Marino',
                'alpha_2'    => 'sm',
                'alpha_3'    => 'smr'
            ),
            array(
                'id'        => 678,
                'name'      => 'Sao Tome and Principe',
                'alpha_2'    => 'st',
                'alpha_3'    => 'stp'
            ),
            array(
                'id'        => 682,
                'name'      => 'Saudi Arabia',
                'alpha_2'    => 'sa',
                'alpha_3'    => 'sau'
            ),
            array(
                'id'        => 686,
                'name'      => 'Senegal',
                'alpha_2'    => 'sn',
                'alpha_3'    => 'sen'
            ),
            array(
                'id'        => 688,
                'name'      => 'Serbia',
                'alpha_2'    => 'rs',
                'alpha_3'    => 'srb'
            ),
            array(
                'id'        => 690,
                'name'      => 'Seychelles',
                'alpha_2'    => 'sc',
                'alpha_3'    => 'syc'
            ),
            array(
                'id'        => 694,
                'name'      => 'Sierra Leone',
                'alpha_2'    => 'sl',
                'alpha_3'    => 'sle'
            ),
            array(
                'id'        => 702,
                'name'      => 'Singapore',
                'alpha_2'    => 'sg',
                'alpha_3'    => 'sgp'
            ),
            array(
                'id'        => 534,
                'name'      => 'Sint Maarten (Dutch part)',
                'alpha_2'    => 'sx',
                'alpha_3'    => 'sxm'
            ),
            array(
                'id'        => 703,
                'name'      => 'Slovakia',
                'alpha_2'    => 'sk',
                'alpha_3'    => 'svk'
            ),
            array(
                'id'        => 705,
                'name'      => 'Slovenia',
                'alpha_2'    => 'si',
                'alpha_3'    => 'svn'
            ),
            array(
                'id'        => 90,
                'name'      => 'Solomon Islands',
                'alpha_2'    => 'sb',
                'alpha_3'    => 'slb'
            ),
            array(
                'id'        => 706,
                'name'      => 'Somalia',
                'alpha_2'    => 'so',
                'alpha_3'    => 'som'
            ),
            array(
                'id'        => 710,
                'name'      => 'South Africa',
                'alpha_2'    => 'za',
                'alpha_3'    => 'zaf'
            ),
            array(
                'id'        => 239,
                'name'      => 'South Georgia and the South Sandwich Islands',
                'alpha_2'    => 'gs',
                'alpha_3'    => 'sgs'
            ),
            array(
                'id'        => 728,
                'name'      => 'South Sudan',
                'alpha_2'    => 'ss',
                'alpha_3'    => 'ssd'
            ),
            array(
                'id'        => 724,
                'name'      => 'Spain',
                'alpha_2'    => 'es',
                'alpha_3'    => 'esp'
            ),
            array(
                'id'        => 144,
                'name'      => 'Sri Lanka',
                'alpha_2'    => 'lk',
                'alpha_3'    => 'lka'
            ),
            array(
                'id'        => 729,
                'name'      => 'Sudan',
                'alpha_2'    => 'sd',
                'alpha_3'    => 'sdn'
            ),
            array(
                'id'        => 740,
                'name'      => 'Suriname',
                'alpha_2'    => 'sr',
                'alpha_3'    => 'sur'
            ),
            array(
                'id'        => 744,
                'name'      => 'Svalbard and Jan Mayen',
                'alpha_2'    => 'sj',
                'alpha_3'    => 'sjm'
            ),
            array(
                'id'        => 752,
                'name'      => 'Sweden',
                'alpha_2'    => 'se',
                'alpha_3'    => 'swe'
            ),
            array(
                'id'        => 756,
                'name'      => 'Switzerland',
                'alpha_2'    => 'ch',
                'alpha_3'    => 'che'
            ),
            array(
                'id'        => 760,
                'name'      => 'Syrian Arab Republic',
                'alpha_2'    => 'sy',
                'alpha_3'    => 'syr'
            ),
            array(
                'id'        => 158,
                'name'      => 'Taiwan, Province of China',
                'alpha_2'    => 'tw',
                'alpha_3'    => 'twn'
            ),
            array(
                'id'        => 762,
                'name'      => 'Tajikistan',
                'alpha_2'    => 'tj',
                'alpha_3'    => 'tjk'
            ),
            array(
                'id'        => 834,
                'name'      => 'Tanzania, United Republic of',
                'alpha_2'    => 'tz',
                'alpha_3'    => 'tza'
            ),
            array(
                'id'        => 764,
                'name'      => 'Thailand',
                'alpha_2'    => 'th',
                'alpha_3'    => 'tha'
            ),
            array(
                'id'        => 626,
                'name'      => 'Timor-Leste',
                'alpha_2'    => 'tl',
                'alpha_3'    => 'tls'
            ),
            array(
                'id'        => 768,
                'name'      => 'Togo',
                'alpha_2'    => 'tg',
                'alpha_3'    => 'tgo'
            ),
            array(
                'id'        => 772,
                'name'      => 'Tokelau',
                'alpha_2'    => 'tk',
                'alpha_3'    => 'tkl'
            ),
            array(
                'id'        => 776,
                'name'      => 'Tonga',
                'alpha_2'    => 'to',
                'alpha_3'    => 'ton'
            ),
            array(
                'id'        => 780,
                'name'      => 'Trinidad and Tobago',
                'alpha_2'    => 'tt',
                'alpha_3'    => 'tto'
            ),
            array(
                'id'        => 788,
                'name'      => 'Tunisia',
                'alpha_2'    => 'tn',
                'alpha_3'    => 'tun'
            ),
            array(
                'id'        => 792,
                'name'      => 'Turkey',
                'alpha_2'    => 'tr',
                'alpha_3'    => 'tur'
            ),
            array(
                'id'        => 795,
                'name'      => 'Turkmenistan',
                'alpha_2'    => 'tm',
                'alpha_3'    => 'tkm'
            ),
            array(
                'id'        => 796,
                'name'      => 'Turks and Caicos Islands',
                'alpha_2'    => 'tc',
                'alpha_3'    => 'tca'
            ),
            array(
                'id'        => 798,
                'name'      => 'Tuvalu',
                'alpha_2'    => 'tv',
                'alpha_3'    => 'tuv'
            ),
            array(
                'id'        => 800,
                'name'      => 'Uganda',
                'alpha_2'    => 'ug',
                'alpha_3'    => 'uga'
            ),
            array(
                'id'        => 804,
                'name'      => 'Ukraine',
                'alpha_2'    => 'ua',
                'alpha_3'    => 'ukr'
            ),
            array(
                'id'        => 784,
                'name'      => 'United Arab Emirates',
                'alpha_2'    => 'ae',
                'alpha_3'    => 'are'
            ),
            array(
                'id'        => 826,
                'name'      => 'United Kingdom of Great Britain and Northern Ireland',
                'alpha_2'    => 'gb',
                'alpha_3'    => 'gbr'
            ),
            array(
                'id'        => 840,
                'name'      => 'United States of America',
                'alpha_2'    => 'us',
                'alpha_3'    => 'usa'
            ),
            array(
                'id'        => 581,
                'name'      => 'United States Minor Outlying Islands',
                'alpha_2'    => 'um',
                'alpha_3'    => 'umi'
            ),
            array(
                'id'        => 858,
                'name'      => 'Uruguay',
                'alpha_2'    => 'uy',
                'alpha_3'    => 'ury'
            ),
            array(
                'id'        => 860,
                'name'      => 'Uzbekistan',
                'alpha_2'    => 'uz',
                'alpha_3'    => 'uzb'
            ),
            array(
                'id'        => 548,
                'name'      => 'Vanuatu',
                'alpha_2'    => 'vu',
                'alpha_3'    => 'vut'
            ),
            array(
                'id'        => 862,
                'name'      => 'Venezuela (Bolivarian Republic of)',
                'alpha_2'    => 've',
                'alpha_3'    => 'ven'
            ),
            array(
                'id'        => 704,
                'name'      => 'Viet Nam',
                'alpha_2'    => 'vn',
                'alpha_3'    => 'vnm'
            ),
            array(
                'id'        => 92,
                'name'      => 'Virgin Islands (British)',
                'alpha_2'    => 'vg',
                'alpha_3'    => 'vgb'
            ),
            array(
                'id'        => 850,
                'name'      => 'Virgin Islands (U.S.)',
                'alpha_2'    => 'vi',
                'alpha_3'    => 'vir'
            ),
            array(
                'id'        => 876,
                'name'      => 'Wallis and Futuna',
                'alpha_2'    => 'wf',
                'alpha_3'    => 'wlf'
            ),
            array(
                'id'        => 732,
                'name'      => 'Western Sahara',
                'alpha_2'    => 'eh',
                'alpha_3'    => 'esh'
            ),
            array(
                'id'        => 887,
                'name'      => 'Yemen',
                'alpha_2'    => 'ye',
                'alpha_3'    => 'yem'
            ),
            array(
                'id'        => 894,
                'name'      => 'Zambia',
                'alpha_2'    => 'zm',
                'alpha_3'    => 'zmb'
            ),
            array(
                'id'        => 716,
                'name'      => 'Zimbabwe',
                'alpha_2'    => 'zw',
                'alpha_3'    => 'zwe'
            ),
        );
    }
}
