<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

use Leonp5\Hybridcms\Models\Content;
use Leonp5\Hybridcms\Models\Page;
use Leonp5\Hybridcms\Models\User;

class PagesContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::find(1);

        Page::truncate();

        $content = new Content();

        $content->setTable('content_gb');
        $content->truncate();
        $content->setTable('content_de');
        $content->truncate();

        DB::table('content_gb')->insert([
            'page_id' => 1,
            'title' => 'About',
            'url' => '/about',
            'content' => 'This is about us',
        ]);

        DB::table('content_gb')->insert([
            'page_id' => 3,
            'title' => 'Disclaimer',
            'url' => '/disclaimer',
            'content' => 'This is the disclaimer page',
        ]);

        DB::table('content_gb')->insert([
            'page_id' => 2,
            'title' => 'Contact',
            'url' => '/contact',
            'content' => 'This is the contact page',
        ]);

        DB::table('content_de')->insert([
            'page_id' => 3,
            'title' => 'Impressum',
            'url' => '/impressum',
            'content' => 'Das ist das Impressum',
        ]);

        DB::table('content_de')->insert([
            'page_id' => 2,
            'title' => 'Kontakt',
            'url' => '/kontakt',
            'content' => 'Das ist die Kontaktseite',
        ]);

        DB::table('content_de')->insert([
            'page_id' => 1,
            'title' => 'Über',
            'url' => '/ueber',
            'content' => 'Das ist die Über Seite',
        ]);

        $publishedStatus = 1;
        $draftStatus = 2;
        $inactiveStatus = 3;

        $aboutPage = Page::create([
            'page_status_id' => $draftStatus,
            'available_lang' => 'de, gb'
        ]);

        $contactPage = Page::create([
            'page_status_id' => $publishedStatus,
            'available_lang' => 'de, gb'
        ]);

        $disclaimerPage = Page::create([
            'page_status_id' => $inactiveStatus,
            'available_lang' => 'de, gb'
        ]);

        $admin->pages()->saveMany([
            $aboutPage,
            $contactPage,
            $disclaimerPage
        ]);
    }
}
