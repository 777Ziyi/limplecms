<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageGb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_gb', function (Blueprint $table) {
            $table->id();
            $table->string('name', 75);
            $table->string('alpha_2', 2)->charset('utf8');
            $table->string('alpha_3', 3)->charset('utf8');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_gb');
    }
}
