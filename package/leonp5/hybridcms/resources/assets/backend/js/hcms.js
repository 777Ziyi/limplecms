import RemoveAlert from './components/RemoveAlert';
import HandleLanguageCheckboxes from './components/HandleLanguageCheckboxes';

document.RemoveAlert = RemoveAlert;

if (
    document.body.contains(document.getElementById('hcms-language-checkboxes'))
) {
    document.HandleLanguageCheckboxes = new HandleLanguageCheckboxes();
}
