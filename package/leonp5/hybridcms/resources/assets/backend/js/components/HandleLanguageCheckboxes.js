export default class HandleLanguageCheckboxes {
    constructor() {
        this.activeLangContainer = document.getElementById(
            'hcms-language-checkboxes'
        )
        this.allLangContainer = document.getElementById('hcms_all_languages')
        this.activeLangsDisablingCheckboxes = this.getActiveLangsDisablingCheckboxes()
        this.allLangDeleteButtons = this.getDeleteButtons()
        this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes()
        this.allLangCheckboxes = this.getAllLanguagesCheckboxes()
        this.updateButtonEnabled = false
        this.mainLangSelected = true
        this.updateButton = document.getElementById('hmcs-update-languages')
        this.activeLangRowPatternElement = this.createLanguageRowPatternElement()

        this.deleteLangButtonClicked = function(e) {
            const tableRow = e.target.closest('tr')
            const langId = tableRow.getAttribute('data-hcms-language-id')
            const allLangCheckbox = this.allLangCheckboxes.find(
                inputEl =>
                    inputEl.getAttribute('data-hcms-language-id') === langId
            )
            allLangCheckbox.checked = false
            tableRow.classList.add('fade')
            window.setTimeout(() => {
                tableRow.style.display = 'none'
                tableRow.remove()
                const activeLangCount = this.activeLangContainer.querySelectorAll(
                    'tr[data-hcms-language-id]'
                ).length
                document.getElementById(
                    'hcms_active_lang_count'
                ).innerHTML = activeLangCount
            }, 300)
            this.enableUpdateButton()
        }

        this.handleLanguageDelete = this.deleteLangButtonClicked.bind(this)

        this.init()
    }

    init() {
        this.activeLangsDisablingCheckboxes.forEach(checkBoxEl => {
            checkBoxEl.addEventListener('click', e => {
                this.handleLanguageDisabling(e)
            })
        })

        this.mainLanguageCheckboxes.forEach(checkBoxEl => {
            checkBoxEl.addEventListener('click', e => {
                this.handleMainLanguageSelection(e)
            })
        })

        document
            .getElementById('all_lang_search')
            .addEventListener('keyup', e => {
                this.handleAllLangSearchFilter(e)
            })

        this.allLangDeleteButtons.forEach(buttonEl => {
            if (!buttonEl.classList.contains('hcms-pointer-forbidden')) {
                buttonEl.addEventListener('click', this.handleLanguageDelete)
            }
        })

        this.allLangCheckboxes.forEach(checkBoxEl => {
            checkBoxEl.addEventListener('click', e => {
                this.handleLanguageAddClick(e)
            })
        })
    }

    createLanguageRowPatternElement() {
        const tableRow = this.activeLangContainer
            .querySelector('tr[data-hcms-language-id]')
            .cloneNode(true)
        const deleteButton = tableRow.querySelector(
            'svg[class="svg-delete hcms-pointer-forbidden"]'
        )
        deleteButton.classList.remove('hcms-pointer-forbidden')
        return tableRow
    }

    handleMainLanguageSelection(e) {
        const deleteButton = e.target
            .closest('tr')
            .querySelector('svg[class^="svg-delete"]')

        if (parseInt(e.target.value) === 1) {
            e.target.value = 0
            e.target.checked = false
            this.mainLangSelected = false
            deleteButton.classList.remove('hcms-pointer-forbidden')
            deleteButton.addEventListener('click', this.handleLanguageDelete)

            this.mainLanguageCheckboxes.forEach(checkbox => {
                const activeLangDisablingCheckbox = checkbox
                    .closest('tr')
                    .querySelector('input[data-hcms-lang-disabled=""]')
                if (activeLangDisablingCheckbox.checked === true) {
                    return
                }
                activeLangDisablingCheckbox.disabled = false
                checkbox.disabled = false
            })
            this.disableUpdateButton()
        } else {
            e.target.value = 1
            e.target.checked = true
            this.mainLangSelected = true
            deleteButton.classList.add('hcms-pointer-forbidden')
            deleteButton.removeEventListener('click', this.handleLanguageDelete)

            this.mainLanguageCheckboxes.forEach(checkbox => {
                if (checkbox === e.target) {
                    const activeLangDisablingCheckbox = checkbox
                        .closest('tr')
                        .querySelector('input[data-hcms-lang-disabled=""]')
                    activeLangDisablingCheckbox.disabled = true
                    return
                }
                checkbox.disabled = true
            })
            this.enableUpdateButton()
        }

        this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes()
    }

    handleLanguageAddClick(e) {
        const langId = e.target.getAttribute('data-hcms-language-id')
        const alreadyAdded = this.activeLangContainer.querySelector(
            `tr[data-hcms-language-id="${langId}"]`
        )

        if (alreadyAdded === null) {
            const langAlpha2Code = e.target.value
            const langName = e.target.name

            const clonedRow = this.activeLangRowPatternElement.cloneNode(true)
            clonedRow.setAttribute('data-hcms-language-id', langId)
            clonedRow.setAttribute('data-hcms-language-alpha_2', langAlpha2Code)

            // set the data from the activated lanuage to the cloned row

            // set flag icon
            clonedRow.getElementsByClassName(
                'flag-icon'
            )[0].className = `flag-icon flag-icon-${langAlpha2Code}`

            // set language name
            clonedRow.querySelector('p[id^="lang_name"]').innerHTML = langName

            // give main lang checkbox correct id value & add eventlistener
            const mainLangCheckbox = clonedRow.querySelector(
                'input[id^="main"]'
            )
            mainLangCheckbox.setAttribute('id', `main_${langId}`)
            if (this.mainLangSelected === true) {
                mainLangCheckbox.disabled = true
            }
            mainLangCheckbox.value = 0
            mainLangCheckbox.checked = false
            mainLangCheckbox.addEventListener('click', e => {
                this.handleMainLanguageSelection(e)
            })

            // update main lang checkbox label
            clonedRow
                .querySelector('label[for^="main"]')
                .setAttribute('for', `main_${langId}`)

            // give lang disabling checkbox correct id, value & add eventlistener
            const activeLangDisablingLangCheckbox = clonedRow.querySelector(
                'input[id^="disabled"]'
            )
            activeLangDisablingLangCheckbox.setAttribute(
                'id',
                `disabled_${langId}`
            )
            activeLangDisablingLangCheckbox.value = 0
            activeLangDisablingLangCheckbox.disabled = false
            activeLangDisablingLangCheckbox.addEventListener('click', e => {
                this.handleLanguageDisabling(e)
            })

            // update disable lang checkbox label
            clonedRow
                .querySelector('label[for^="disabled"]')
                .setAttribute('for', `disabled_${langId}`)

            // add delete function to delete button
            clonedRow
                .querySelector('svg[name="delete"]')
                .addEventListener('click', e => {
                    this.handleLanguageDelete(e)
                })

            this.activeLangContainer.querySelector('tbody').append(clonedRow)
        } else {
            this.activeLangContainer
                .querySelector(`tr[data-hcms-language-id="${langId}"]`)
                .remove()
        }

        const activeLangCount = this.activeLangContainer.querySelectorAll(
            'tr[data-hcms-language-id]'
        ).length
        document.getElementById(
            'hcms_active_lang_count'
        ).innerHTML = activeLangCount

        this.enableUpdateButton()
        this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes()
    }

    handleLanguageDisabling(e) {
        const tableRow = e.target.closest('tr')
        const mainLangCheckbox = tableRow.querySelector(
            'input[data-hcms-main-language=""]'
        )
        const mainLanguageChecked = this.isMainLangCheckboxChecked()
        if (parseInt(e.target.value) === 1) {
            e.target.value = 0
            e.target.checked = false
            tableRow.classList.remove('bg-light')
            tableRow.classList.remove('text-dark')

            if (mainLanguageChecked === false) {
                mainLangCheckbox.disabled = false
            }

            if (parseInt(mainLangCheckbox.value) === 1) {
                this.activeLangContainer.classList.remove('border-warning')
            }
        } else {
            e.target.value = 1
            e.target.checked = true
            tableRow.classList.add('bg-light')
            tableRow.classList.add('text-dark')

            if (mainLanguageChecked === false) {
                mainLangCheckbox.disabled = true
            }

            if (parseInt(mainLangCheckbox.value) === 1) {
                this.activeLangContainer.classList.add('border-warning')
            }
        }

        this.activeLangsDisablingCheckboxes = this.getActiveLangsDisablingCheckboxes()

        let leftLanguageInputs = []
        this.activeLangsDisablingCheckboxes.forEach(element => {
            if (parseInt(element.value) === 0) {
                leftLanguageInputs.push(element)
            }
        })

        if (1 > leftLanguageInputs.length) {
            this.activeLangContainer.classList.add('border-danger')
            this.disableUpdateButton()
        }

        if (leftLanguageInputs.length >= 1) {
            this.activeLangContainer.classList.remove('border-danger')
            this.enableUpdateButton()
        }
    }

    getActiveLangsDisablingCheckboxes() {
        return Array.from(
            this.activeLangContainer.querySelectorAll(
                'input[data-hcms-lang-disabled=""]'
            )
        )
    }

    getMainLanguageCheckboxes() {
        return Array.from(
            this.activeLangContainer.querySelectorAll(
                'input[data-hcms-main-language=""]'
            )
        )
    }

    getAllLanguagesCheckboxes() {
        return Array.from(
            this.allLangContainer.querySelectorAll(
                'input:not([id="all_lang_search"])'
            )
        )
    }

    getDeleteButtons() {
        return Array.from(
            this.activeLangContainer.querySelectorAll('svg[name=delete]')
        )
    }

    handleAllLangSearchFilter(e) {
        const searchString = e.target.value.toUpperCase()
        for (let i = 0; i < this.allLangCheckboxes.length; i++) {
            const langName = this.allLangCheckboxes[i].name
            if (langName.toUpperCase().indexOf(searchString) > -1) {
                this.allLangCheckboxes[i].parentNode.parentNode.style.display =
                    ''
            } else {
                this.allLangCheckboxes[i].parentNode.parentNode.style.display =
                    'none'
            }
        }
        const visibleLangsCount = this.allLangContainer.querySelectorAll(
            'div[name="hcms_lang_container"]:not([style="display: none;"])'
        ).length

        const allLangCountElement = document.getElementById(
            'hcms_all_lang_count'
        )

        allLangCountElement.innerHTML = visibleLangsCount
    }

    enableUpdateButton() {
        if (
            this.updateButtonEnabled === false &&
            this.mainLangSelected === true
        ) {
            this.updateButton.removeAttribute('disabled')
            this.updateButton.classList.remove('hcms-pointer-forbidden')
            this.updateButtonEnabled = true
        }
    }

    disableUpdateButton() {
        if (this.updateButtonEnabled === true) {
            this.updateButton.setAttribute('disabled', 'disabled')
            this.updateButton.classList.add('hcms-pointer-forbidden')
            this.updateButtonEnabled = false
        }
    }

    isMainLangCheckboxChecked() {
        let checkedBox = []

        this.mainLanguageCheckboxes.forEach(checkbox => {
            if (checkbox.checked === true) {
                checkedBox.push(checkbox)
            }
        })
        if (checkedBox.length > 0) {
            return true
        }
        return false
    }
}
