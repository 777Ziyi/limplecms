<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | navigation.
    |
    */

    'pages' => 'Pages',
    'pages.overview' => 'Pages overview',
    'pages.create' => 'Create new page',

    'user.management' => 'User management',
    'users.overview' => 'All users',
    'users.create' => 'Add new user',

    'languages' => 'Languages',
    'languages.settings' => 'Language settings',

    'user.dropdown.settings' => 'Settings',
    'user.dropdown.account' => 'Account',
    'user.dropdown.user' => 'User'
];
