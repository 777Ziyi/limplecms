<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Frontend Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for all translaitions concerning the frontend.
    |
    */

    'page.not.found' => 'The requested page is no longer here or does not exist.',
    'page.not.found.button' => 'To the home page'
];
