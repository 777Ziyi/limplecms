<?php

return [

    // Pages
    'title.pages.index' => 'Pages overview',
    'title.page.edit' => 'Edit page',
    'title.page.create' => 'Create page',

    // Users
    'title.users.index' => 'User overview',
    'title.users.edit' => 'Edit ":username"',
    'title.users.add' => 'Add user',

    // Languages
    'title.languages.index' => 'Language settings'
];
