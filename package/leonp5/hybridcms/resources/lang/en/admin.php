<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | admin dashboard and actions.
    |
    */

    // General
    'update.data.success' => 'Changes successful saved!',


    // Pages table
    'create.page' => 'Create new site',
    'page.status.draft' => 'Draft',
    'page.status.inactive' => 'Inactive',
    'page.status.published' => 'Published',
    'page.title' => 'Page title',
    'page.no-pages' => 'There are no pages',
    'page.available.lang' => 'Available Languages',
    'page.title.missing' => 'Missing title',
    'page.url.missing' => 'Missing url',

    // Page create, edit & delete
    'edit.page' => 'Edit page',
    'edit.title' => 'Page title',
    'edit.url' => 'URL',
    'edit.content' => 'Page content',
    'edit.submit' => 'Save changes',
    'edit.status' => 'Status',

    'create.submit' => 'Create page',
    'edit.abort' => 'Cancel',

    'delete.modal.title' => 'Confirm deletion',
    'delete.modal.text' => 'Permanently delete this page?',
    'delete.abort' => 'Cancel',
    'delete.page.confirm' => 'Delete page',

    'page.update.success' => 'Page update saved!',
    'page.save.success' => 'Page saved!',

    'page.delete.success' => 'Page deleted!',

    // url error messages
    'invalid.url' => 'Invalid URL',
    'url.already.exists' => 'URL already exists for the language ":language"',
    'min.one.url' => 'The page needs at least in one language a valid url',
    'page.need.different.urls' => 'It is not allowed, that the url is identical with one url of this page in another language',

    'page.missing.title.min' => 'The page needs at least in one language a title',

    // user table
    'table.users.no-users' => 'There are no users',
    'table.user.name' => 'Username',
    'table.user.role' => 'Role',
    'table.user.last-online' => 'Last online',

    // user create, edit & delete
    'add.user' => 'Add new user',
    'delete.user.modal.text' => 'Delete the user ":user"?',
    'delete.user.confirm' => 'Delete user',
    'delete.user.success' => '":username" deleted!',
    'edit.user' => 'Edit ":username"',
    'edit.user.role' => 'Role(s)',
    'edit.user.username' => 'Username',
    'edit.user.basic.data' => 'Basic data',
    'edit.user.new.pw' => 'New password',
    'edit.user.new.pw.confirm' => 'Confirm password',
    'save.user.admin.role.error' => 'You can\'t delete your own admin rights!',
    'save.user.missing.role.error' => 'A user needs at least one role!',
    'save.user.short.name.error' => 'A username needs at least :minLength characters!',
    'save.user.name.exists.error' => 'The username ":name" already exists!',
    'save.user.pw.empty.error' => 'Both password fields are required!',
    'save.user.pw.not.identic.error' => 'The entered Passwords aren\'t identical!',
    'save.user.short.pw.error' => 'The password must have at least :minLength characters!',
    'save.user.capital.pw.error' => 'The password must contain at least one capital letter!',
    'save.user.number.pw.error' => 'The password must contain at least one number!',
    'save.user.success' => 'User successfully created!',
    'create.user' => 'Create new user',
    'create.user.submit' => 'Create user',
    'create.user.abort' => 'Cancel',
    'create.user.username' => 'Username',
    'create.user.new.pw' => 'New password',
    'create.user.new.pw.confirm' => 'Confirm password',
    'create.user.role' => 'Role(s)',


    // user roles
    'user.role.admin' => 'Admin',
    'user.role.editor' => 'Editor',
    'user.role.author' => 'Author',

    // Languages
    'languages.multilang.disabled' => 'Multilanguage disabled',
    'languages.active.heading' => 'Active languages',
    'languages.all.heading' => 'All languages',
    'languages.settings' => 'Language settings',
    'languages.table.heading.language' => 'Language',
    'languages.table.heading.main-language' => 'Main language',
    'languages.table.heading.disable' => 'Disable',
    'languages.table.heading.delete' => 'Delete',
    'languages.table.button.update' => 'Save',
];
