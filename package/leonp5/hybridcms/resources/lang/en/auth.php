<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'register.username' => 'Username',
    'register.email' => 'E-Mail Address',
    'register.pw' => 'Password',
    'register.cpw' => 'Confirm Password',
    'register' => 'Register',

    'login.login' => 'Login',
    'login.username' => 'Username',
    'login.password' => 'Password',
    'login.remember' => 'Remember',
    'login.missing.password' => 'Please enter a password',
    'login.missing.identity' => 'Please enter a user name',

    'logout.successful' => 'Logout successful!'

];
