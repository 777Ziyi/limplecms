<?php

return [

    // Pages
    'title.pages.index' => 'Seitenübersicht',
    'title.page.edit' => 'Seite bearbeiten',
    'title.page.create' => 'Seite erstellen',

    // Users
    'title.users.index' => 'Benutzerübersicht',
    'title.users.edit' => '":username" bearbeiten',
    'title.users.add' => 'Benutzer hinzufügen',

    // Languages
    'title.languages.index' => 'Spracheinstellungen'
];
