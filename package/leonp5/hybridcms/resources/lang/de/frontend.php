<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Frontend Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for all translaitions concerning the frontend.
    |
    */

    'page.not.found' => 'Die angefragte Seite ist nicht mehr hier oder existiert nicht.',
    'page.not.found.button' => 'Zur Startseite'
];
