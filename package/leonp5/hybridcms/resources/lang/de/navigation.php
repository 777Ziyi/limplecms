<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | navigation.
    |
    */

    'pages' => 'Seiten',
    'pages.overview' => 'Seitenübersicht',
    'pages.create' => 'Neue Seite anlegen',

    'user.management' => 'Benutzerverwaltung',
    'users.overview' => 'Alle Benutzer',
    'users.create' => 'Benutzer anlegen',

    'languages' => 'Sprachauswahl',
    'languages.settings' => 'Spracheinstellungen',

    'user.dropdown.settings' => 'Einstellungen',
    'user.dropdown.account' => 'Account',
    'user.dropdown.user' => 'Benutzer'

];
