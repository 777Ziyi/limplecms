<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | admin dashboard and actions.
    |
    */

    // General
    'update.data.success' => 'Änderungen erfolgreich gespeichert!',

    // Pages table
    'create.page' => 'Neue Seite anlegen',
    'page.status.draft' => 'Entwurf',
    'page.status.inactive' => 'Deaktiviert',
    'page.status.published' => 'Veröffentlicht',
    'page.title' => 'Seitentitel',
    'page.no-pages' => 'Es gibt noch keine Seiten',
    'page.available.lang' => 'Verfügbare Sprachen',
    'page.title.missing' => 'Titel fehlt',
    'page.url.missing' => 'URL fehlt',

    // Page create, edit & delete
    'edit.page' => 'Seite bearbeiten',
    'edit.title' => 'Seitentitel',
    'edit.url' => 'URL',
    'edit.content' => 'Seiteninhalt',
    'edit.submit' => 'Speichern',
    'edit.status' => 'Status',

    'create.submit' => 'Seite anlegen',
    'edit.abort' => 'Abbrechen',

    'delete.modal.title' => 'Löschvorgang bestätigen',
    'delete.modal.text' => 'Seite unwiderruflich löschen?',
    'delete.abort' => 'Abbrechen',
    'delete.page.confirm' => 'Seite löschen',

    'page.update.success' => 'Seite aktualisiert!',
    'page.save.success' => 'Seite gespeichert!',

    'page.delete.success' => 'Seite gelöscht!',

    // url error messages
    'invalid.url' => 'Ungültige URL',
    'url.already.exists' => 'Die URL existiert bereits in der Sprache ":language"',
    'min.one.url' => 'Die Seite braucht in mindestens einer Sprache eine gültige URL',
    'page.need.different.urls' => 'Die URL darf nicht mit der URL dieser Seite in einer anderen Sprache identisch sein',

    'page.missing.title.min' => 'Die Seite braucht in mindestens einer Sprache einen Titel',

    // user table
    'table.users.no-users' => 'Es gibt noch keine Benutzer',
    'table.user.name' => 'Benutzername',
    'table.user.role' => 'Rolle',
    'table.user.last-online' => 'Zuletzt online',

    // user create, edit & delete
    'add.user' => 'Neuen Benutzer anlegen',
    'delete.user.modal.text' => 'Den Benutzer ":user" löschen?',
    'delete.user.confirm' => 'Benutzer löschen',
    'delete.user.success' => '":username" gelöscht!',
    'edit.user' => '":username" bearbeiten',
    'edit.user.role' => 'Rolle(n)',
    'edit.user.username' => 'Benutzername',
    'edit.user.basic.data' => 'Basisdaten',
    'edit.user.new.pw' => 'Neues Passwort',
    'edit.user.new.pw.confirm' => 'Passwort bestätigen',
    'save.user.admin.role.error' => 'Du kannst deine Adminrechte nicht löschen!',
    'save.user.missing.role.error' => 'Ein Benutzer braucht mindestens eine Rolle!',
    'save.user.short.name.error' => 'Es muss ein Benutzername mit mindestes :minLength vergeben werden!',
    'save.user.name.exists.error' => 'Der Benutzername ":name" existiert bereits',
    'save.user.pw.empty.error' => 'Beide Passwort Felder müssen ausgefüllt werden!',
    'save.user.pw.not.identic.error' => 'Die eingegeben Passwörter stimmen nicht überein!',
    'save.user.short.pw.error' => 'Das Passwort muss mindestens :minLength Zeichen lang sein!',
    'save.user.capital.pw.error' => 'Das Passwort muss mindestens einen Großbuchstaben enthalten!',
    'save.user.number.pw.error' => 'Das Passwort muss mindestens eine Zahl enthalten!',
    'save.user.success' => 'Benutzer erfolgreich angelegt!',
    'create.user' => 'Neuen Benutzer anlegen',
    'create.user.submit' => 'Benutzer anlegen',
    'create.user.username' => 'Benutzername',
    'create.user.abort' => 'Abbrechen',
    'create.user.new.pw' => 'Neues Passwort',
    'create.user.new.pw.confirm' => 'Passwort bestätigen',
    'create.user.role' => 'Rolle(n)',

    // user roles
    'user.role.admin' => 'Administrator',
    'user.role.editor' => 'Editor',
    'user.role.author' => 'Autor',

    // Languages
    'languages.multilang.disabled' => 'Mehrsprachigkeit nicht aktiviert',
    'languages.active.heading' => 'Aktivierte Sprachen',
    'languages.all.heading' => 'Alle Sprachen',
    'languages.settings' => 'Spracheinstellungen',
    'languages.table.heading.language' => 'Sprache',
    'languages.table.heading.main-language' => 'Hauptsprache',
    'languages.table.heading.disable' => 'Deaktivieren',
    'languages.table.heading.delete' => 'Löschen',
    'languages.table.button.update' => 'Speichern',
];
