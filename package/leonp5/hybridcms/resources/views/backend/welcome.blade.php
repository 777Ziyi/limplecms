@extends('hybridcms::backend.layout.app')

@section('title')
hybridCms dev app
@endsection

@section('content')
    
    <div class="d-flex justify-content-center">
        <div class="d-inline-flex p-2 border">
            @if (Route::has('login'))
                <div class="mr-3">
                    @auth
                        <a href="{{ route('dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                    @endif
                </div>
            @endif


                    <div>
                        Laravel Build v{{ Illuminate\Foundation\Application::VERSION }}
                    </div>
                </div>
        </div>
    </div>
        @endsection
