@csrf

<div class="card mt-4">
    <div class="card-header">
        <p class="h5">{{__('hybridcms::admin.edit.user.basic.data')}}</p>
    </div>
    <div class="card-body">
        @if($user ?? '')
        <div class="form-group row">
            <label for="lcms-username-input"
                class="col-sm-3 col-form-label">{{__('hybridcms::admin.edit.user.username')}}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="username" id="lcms-username-input"
                    value="{{ old('username', $user->username)}}">
                    <input type="hidden" name="user_uuid" value="{{$user->uuid}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="lcms-password-input"
                class="col-sm-3 col-form-label">{{__('hybridcms::admin.edit.user.new.pw')}}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="password" id="lcms-password-input" value="">
            </div>
        </div>
        <div class="form-group row">
            <label for="lcms-cpassword-input"
                class="col-sm-3 col-form-label">{{__('hybridcms::admin.edit.user.new.pw.confirm')}}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="cpassword" id="lcms-cpassword-input" value="">
            </div>
        </div>
        <div class="dropdown-divider"></div>
        <p class="h4 mt-4 mb-3">{{__('hybridcms::admin.edit.user.role')}}</p>

        @foreach ($roles as $role)
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="roles[]" value="{{$role->id}}" {{!empty ($role->userHas) ? 'checked' : ''}}>
            <label class="form-check-label">{{__('hybridcms::admin.user.role.' .$role->role)}}</label>
        </div>
        @endforeach

        @else

        <div class="form-group row">
            <label for="lcms-username-input"
                class="col-sm-3 col-form-label">{{__('hybridcms::admin.create.user.username')}}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="username" id="lcms-username-input"
                    value="{{ old('username')}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="lcms-password-input"
                class="col-sm-3 col-form-label">{{__('hybridcms::admin.create.user.new.pw')}}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="password" id="lcms-password-input" value="">
            </div>
        </div>
        <div class="form-group row">
            <label for="lcms-cpassword-input"
                class="col-sm-3 col-form-label">{{__('hybridcms::admin.create.user.new.pw.confirm')}}</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" name="cpassword" id="lcms-cpassword-input" value="">
            </div>
        </div>
        <div class="dropdown-divider"></div>
        <p class="h4 mt-4 mb-3">{{__('hybridcms::admin.create.user.role')}}</p>

        @foreach ($roles as $role)
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" name="roles[]" value="{{$role->id}}"
                @if(old('roles[]')==$role->id)
            checked @endif>
            <label class="form-check-label">{{__('hybridcms::admin.user.role.' .$role->role)}}</label>
        </div>
        @endforeach
        @endif
    </div>
</div>