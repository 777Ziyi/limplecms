@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.users.edit', ['username' => $user->username]))

@section('content')

<div class="container">
    <h3>{{__('hybridcms::admin.edit.user', ['username' => $user->username])}}</h3>

    <form action="{{route('users.update', ['user' => $user->uuid])}}" method="POST">
        @method('PUT')

        @include('hybridcms::backend.admin.users.partials.fields')
        
        <div class="form-group d-flex justify-content-between">
            <a href="{{ url()->previous() }}" class="btn btn-secondary">{{__('hybridcms::admin.edit.abort')}}</a>
            <button type="submit" class="btn btn-primary">{{__('hybridcms::admin.edit.submit')}}</button>
        </div>
    </form>

</div>

@endsection