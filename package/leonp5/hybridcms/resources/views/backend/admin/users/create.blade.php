@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.users.add'))

@section('content')

<div class="container">
    <h3>{{__('hybridcms::admin.create.user')}}</h3>

    <form action="{{route('users.store')}}" method="POST">
       
        @include('hybridcms::backend.admin.users.partials.fields')
        
        <div class="form-group d-flex justify-content-between">
            <a href="{{ url()->previous() }}" class="btn btn-secondary">{{__('hybridcms::admin.create.user.abort')}}</a>
            <button type="submit" class="btn btn-primary">{{__('hybridcms::admin.create.user.submit')}}</button>
        </div>
    </form>

</div>

@endsection