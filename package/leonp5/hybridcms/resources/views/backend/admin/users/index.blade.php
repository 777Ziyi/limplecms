@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.users.index'))


@section('content')

<div class="container">
  <a class="btn btn-primary mb-3" href="{{route('users.create')}}">{{__('hybridcms::admin.add.user')}}</a>
  @if(count($allUsers) > 0)
  <table class="table">
    <thead>
      <tr>
        <th scope="col">{{__('hybridcms::admin.table.user.name')}}</th>
        <th scope="col">{{__('hybridcms::admin.table.user.role')}}</th>
        <th scope="col">{{__('hybridcms::admin.table.user.last-online')}}</th>
        <th scope="col">
          
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($allUsers as $user)
      <tr>
        <td>
          <a href="{{route('users.edit', ['user' => $user->uuid])}}">
            {{$user->username}}</a>
        </td>
      <td>{{ $user->roles}}</td>
      {{-- <td class="td">{{ implode(", ", $user->roles()->get()->pluck("role")->toArray()) }}</td> --}}
      <td>{{$user->last_online_at}}</td>

        <td>
          <a type="button" data-toggle="modal" data-target="{{'#deleteConfirmModal' . $user->uuid}}">
            <x-hybridcms-svg svg="trash" fill="none" class="svg-delete"/></td>
          </a>
          <div class="modal fade" id="{{'deleteConfirmModal'. $user->uuid}}" tabindex="-1" aria-labelledby="deleteConfirmModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="deleteConfirmModalLabel">{{__('hybridcms::admin.delete.modal.title')}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                 <div class="row m-2"> 
                   {{__('hybridcms::admin.delete.user.modal.text', ['user' => $user->username])}} 
                </div> 
                 <div class="row m-2">
                   <div class="col">
                  <button type="button" class="btn btn-secondary m-2" data-dismiss="modal">{{__('hybridcms::admin.delete.abort')}}</button>
                </div>
                  <div class="col align-self-end">
                    <form method="POST" action="{{route('users.destroy', [$user->uuid])}}">
                      {{csrf_field()}}
                      <input type="hidden" name="_method" value="DELETE">
                      <button type="submit" class="btn btn-danger m-2">{{__('hybridcms::admin.delete.user.confirm')}}</button>
                    </form>
                  </div>
                </div>
                {{-- <div class="modal-footer">
                  
                </div> --}}
              </div>
            </div>
          </div>
      </tr>
      @endforeach
    </tbody>
  </table>
  @else
  <div class="d-flex justify-content-center">
    <h4 class='text-info'>{{__('hybridcms::admin.table.users.no-users')}}</h5>
  </div>
  @endif
</div>
@endsection
