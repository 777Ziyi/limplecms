@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.languages.index'))

@if(count($activeLanguages) > 0)
@section('styles')
<link href="{{ asset('backend/flags.css') }}" rel="stylesheet">
@endsection
@endif

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h3 class="mb-3">{{__('hybridcms::admin.languages.settings')}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @if(count($activeLanguages) > 0)
            <form class="card" id="hcms-language-checkboxes" action="{{route('languages.store')}}" method="POST">
                @csrf
                <h5 class="card-header">{{__('hybridcms::admin.languages.active.heading')}}
                    <span class="badge badge-pill badge-primary float-right"
                        id="hcms_active_lang_count">{{count($activeLanguages)}}</span>
                </h5>
                <div class="card-body">
                    <table class="table">
                        <thead class="table-light">
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.language')}}</th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.main-language')}}</th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.disable')}}</th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($activeLanguages as $language)
                            <tr class="{{$language->disabled ? "bg-light text-dark" : ""}}"
                                data-hcms-language-id="{{$language->id}}"
                                data-hcms-language-alpha_2="{{$language->alpha_2}}">
                                <input type="hidden" name="languages[{{$language->lang_id}}][alpha_2]"
                                    value="{{$language->alpha_2}}" />
                                <td>
                                    <span class="flag-icon flag-icon-{{$language->alpha_2}}"></span>
                                </td>
                                <td>
                                    <p id="lang_name_{{$language->alpha_2}}">{{$language->langName}}</p>
                                </td>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input"
                                            id="main_{{$language->lang_id}}"
                                            {{$language->main_language ? "checked" : "disabled"}}
                                            value="{{$language->main_language}}"
                                            name="languages[{{$language->lang_id}}][main_language]"
                                             data-hcms-main-language="">
                                        <label class="custom-control-label" for="main_{{$language->lang_id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input"
                                            id="disabled_{{$language->lang_id}}"
                                            {{$language->main_language ? "disabled" : ""}}
                                            {{$language->disabled ? "checked" : ""}} value="{{$language->disabled}}"
                                            name="languages[{{$language->lang_id}}][disabled]" data-hcms-lang-disabled="">
                                        <label class="custom-control-label"
                                            for="disabled_{{$language->lang_id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <x-hybridcms-svg svg="trash" fill="none"
                                        class="{{$language->main_language ? 'svg-delete hcms-pointer-forbidden' : 'svg-delete'}}"
                                        name="delete" />
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-success hcms-pointer-forbidden" type="submit" disabled
                            id="hmcs-update-languages">
                            {{__('hybridcms::admin.languages.table.button.update')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">{{__('hybridcms::admin.languages.all.heading')}}
                    <span class="badge badge-pill badge-primary float-right"
                        id="hcms_all_lang_count">{{count($allLanguages)}}</span>
                </h5>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col">
                            <div class="input-group">
                                <input class="form-control" id="all_lang_search">
                                <span class="input-group-append">
                                    <button class="btn" type="button">
                                        <x-hybridcms-svg svg="magnifying-glass" viewBox="512 512" />
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap" id="hcms_all_languages">
                        @foreach($allLanguages as $language)
                        <div class="col-6 col-md-4 col-lg-3" name="hcms_lang_container">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="lang_{{$language->id}}"
                                    {{isset($language->enabled) ? "checked" : ""}}
                                    data-hcms-language-id="{{$language->id}}" name="{{$language->name}}"
                                    value="{{$language->alpha_2}}">
                                <label class="custom-control-label"
                                    for="lang_{{$language->id}}">{{$language->name}}</label>
                                <span class="flag-icon flag-icon-{{$language->alpha_2}}"></span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <h4 class="text-center">{{__('hybridcms::admin.languages.multilang.disabled')}}</h4>
    @endif
</div>
@endsection