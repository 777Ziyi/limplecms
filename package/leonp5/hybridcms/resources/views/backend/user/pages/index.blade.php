@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.pages.index'))

@section('content')

<div class="container">
  @canany(['admin', 'editor'])
  <a class="btn btn-primary mb-3" href="{{route('pages.create')}}">{{__('hybridcms::admin.create.page')}}</a>
  @endcanany
  @if(count($allPages) > 0)
  <table class="table table-responsive-sm">
    <thead>
      <tr>
        <th scope="col">{{__('hybridcms::admin.page.title')}}</th>
        <th scope="col">URL</th>
        <th scope="col">Status</th>
        <th scope="col">{{__('hybridcms::admin.page.available.lang')}}</th>
        @canany(['admin', 'editor'])
        <th scope="col"></th>
        @endcanany
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($allPages as $page)
      <tr>

        <td>
          <a href="{{route('pages.edit', ['page' => $page->id])}}">
            {{$page->title !== null ? $page->title : __('hybridcms::admin.page.title.missing')}}</a>
        </td>

        <td>{{ $page->url !== null ? $page->url : __('hybridcms::admin.page.url.missing')}}</td>

        @if($page->page_status_id === 2)
        <td>
          <span class="badge badge-warning">
            {{__('hybridcms::admin.page.status.draft')}}
          </span></td>
        @elseif ($page->page_status_id === 3)
        <td>
          <span class="badge badge-secondary">
            {{__('hybridcms::admin.page.status.inactive')}}
          </span></td>
        @elseif($page->page_status_id === 1)
        <td>
          <span class="badge badge-success">{{__('hybridcms::admin.page.status.published')}}
          </span></td>
        @endif

        <td>{{$page->available_lang}}</td>

        @canany(['admin', 'editor'])
        <td>
          <a type="button" data-toggle="modal" data-target="{{'#deleteConfirmModal' . $page->id}}">
            <x-hybridcms-svg svg="trash" fill="none" class="svg-delete"/></td>
          </a>
          <div class="modal fade" id="{{'deleteConfirmModal'. $page->id}}" tabindex="-1" aria-labelledby="deleteConfirmModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="deleteConfirmModalLabel">{{__('hybridcms::admin.delete.modal.title')}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                 <div class="row m-2"> {{__('hybridcms::admin.delete.modal.text')}}</div> 
                 <div class="row m-2">
                   <div class="col">
                  <button type="button" class="btn btn-secondary m-2" data-dismiss="modal">{{__('hybridcms::admin.delete.abort')}}</button>
                </div>
                  <div class="col align-self-end">
                    <form method="POST" action="{{route('pages.destroy', [$page->id])}}">
                      {{csrf_field()}}
                      <input type="hidden" name="_method" value="DELETE">
                      <button type="submit" class="btn btn-danger m-2">{{__('hybridcms::admin.delete.page.confirm')}}</button>
                    </form>
                  </div>
                </div>
                {{-- <div class="modal-footer">
                  
                </div> --}}
              </div>
            </div>
          </div>
        </td>
        @endcanany
      </tr>
      @endforeach
    </tbody>
  </table>
  
  {{$allPages->links()}}
  @else
  <div class="d-flex justify-content-center">

    <h4 class='text-info'>{{__('hybridcms::admin.page.no-pages')}}</h5>
  </div>
  @endif
</div>
@endsection
