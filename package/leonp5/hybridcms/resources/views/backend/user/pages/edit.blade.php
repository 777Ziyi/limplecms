@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.page.edit'))

@section('content')

<div class="container">
    <h3>{{__('hybridcms::admin.edit.page')}}</h3>

    <form action="{{route('pages.update', ['page' => $page->id])}}" method="POST">
        @method('PUT')

        @include('hybridcms::backend.user.pages.partials.fields')
        
        <div class="form-group d-flex justify-content-between">
            <a href="{{ url()->previous() }}" class="btn btn-secondary">{{__('hybridcms::admin.edit.abort')}}</a>
            <button type="submit" class="btn btn-primary">{{__('hybridcms::admin.edit.submit')}}</button>
        </div>
    </form>

</div>

@endsection