@csrf
<div class="form-row mb-3">
    <div class="col">
        <label class="label-m mt-3" for="page_status">@lang('hybridcms::admin.edit.status')</label>
        <select class="form-control" id="page_status" name="page_status">
            @if ($page ?? '')
            <option value="{{$page->page_status_id}}">{{$page->status_label}}</option>
            @foreach ($statusLabels as $status)
            <option value="{{$status->id}}">{{$status->status_label}}</option>
            @endforeach
            @else
            @foreach ($statusLabels as $status)
            <option value="{{$status->id}}" @if(old('page_status') == $status->id) selected="selected" @endif>
                {{$status->status_label}}</option>
            @endforeach
            @endif
        </select>
    </div>
</div>

@if(empty($page))

@foreach($activeLanguages as $activeLang)

<p class="h4 mt-5">{{$activeLang->langName}}</p>
<input type="hidden" name="languages[]" value="{{$activeLang->alpha_2}}">
<div class="form-row">
    <div class="col">
        <label class="label-m mt-3" for="{{'title_' . $activeLang->alpha_2}}">@lang('hybridcms::admin.edit.title')</label>
        <input type="text" class="form-control" id="{{'title_' . $activeLang->alpha_2}}" 
        name="{{'title_' . $activeLang->alpha_2}}" value="{{old('title_' . $activeLang->alpha_2)}}" />
    </div>
    <div class="col">
        <label class="label-m mt-3" for="{{'url_' . $activeLang->alpha_2}}">@lang('hybridcms::admin.edit.url')</label>
        <input type="text" class="form-control" id="{{'url_' . $activeLang->alpha_2}}"
         name="{{'url_' . $activeLang->alpha_2}}" value="{{old('url_' . $activeLang->alpha_2)}}" />
    </div>
</div>
<div class="form-group">
    <label class="label-m mt-3" for="{{'content_' . $activeLang->alpha_2}}">@lang('hybridcms::admin.edit.content')</label>
    <textarea type="text" class="form-control" id="{{'content_' . $activeLang->alpha_2}}"
        name="{{'content_' . $activeLang->alpha_2}}">{{old('content_' . $activeLang->alpha_2)}}</textarea>
</div>

@endforeach

@else

@foreach ($page->langs as $pageContent)
<p class="h4 mt-5">{{$pageContent->langName}}</p>
<input type="hidden" name="languages[]" value="{{$pageContent->lang}}">
<div class="form-row">
    <div class="col">
        <label class="label-m mt-3" for="title">{{__('hybridcms::admin.edit.title')}}</label>
        <input type="text" class="form-control" id="title" name="{{'title_' . $pageContent->lang}}"
            value="{{$pageContent->title ?? ''}}" />
    </div>
    <div class="col">
        <label class="label-m mt-3" for="url">{{__('hybridcms::admin.edit.url')}}</label>
        <input type="text" class="form-control" id="url" name="{{'url_' . $pageContent->lang}}"
            value="{{$pageContent->url ?? ''}}" />
    </div>
</div>
<div class="form-group">
    <label class="label-m mt-3" for="content">{{__('hybridcms::admin.edit.content')}}</label>
    <textarea type="text" class="form-control" id="content"
    name="{{'content_' . $pageContent->lang}}">{{$pageContent->content ?? ''}}
    </textarea>
</div>

@endforeach

@endif