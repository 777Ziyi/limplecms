<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if(View::hasSection('title'))
        @yield('title')
        @else
        hybridCms dev app
        @endif
    </title>

    <link href="{{ asset('backend/hcms.css') }}" rel="stylesheet">
    @yield('styles')
    
</head>

<body>
    @auth
    @include('hybridcms::backend.layout.partials.sidebar')  
    @endauth
    <div class="c-wrapper c-fixed-components">
        @include('hybridcms::backend.layout.partials.nav')
        <div class="c-body">
            <main class="c-main">
                @include('hybridcms::backend.layout.partials.alerts')
                @yield('content')
            </main>
        </div>
    </div>
    <script src="{{ asset('backend/hcms.js') }}" defer></script>
    <script src="{{ asset('backend/hcms-vendor.js')}}"></script>
</body>

</html>