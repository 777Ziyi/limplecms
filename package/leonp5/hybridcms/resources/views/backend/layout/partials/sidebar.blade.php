{{-- <div class="c-sidebar c-sidebar-dark c-sidebar-show"> --}}
<div id="sidebar" class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show">
  <div class="c-sidebar-brand d-md-down-none">
    hybridCMS
  </div>
  <ul class="c-sidebar-nav">
    <li class="c-sidebar-nav-title">
      {{ __('hybridcms::navigation.pages')}}</li>
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="{{ route('pages.index') }}">
        <x-hybridcms-svg svg="library" class="c-sidebar-nav-icon" viewBox="512 512" />
        {{__('hybridcms::navigation.pages.overview')}}
      </a>
    </li>
    @canany(['admin', 'editor'])
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="{{ route('pages.create') }}">
        <x-hybridcms-svg svg="library-add" class="c-sidebar-nav-icon" viewBox="512 512" />
        {{__('hybridcms::navigation.pages.create')}}
      </a>
    </li>
    @endcanany
    @can('admin')
    <li class="c-sidebar-nav-title">
      {{ __('hybridcms::navigation.user.management')}}
    </li>
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="{{ route('users.index') }}">
        <x-hybridcms-svg svg="group" class="c-sidebar-nav-icon" viewBox="512 512" />
        {{__('hybridcms::navigation.users.overview')}}
      </a>
    </li>
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="{{ route('users.create') }}">
        <x-hybridcms-svg svg="user-add" class="c-sidebar-nav-icon" viewBox="512 512" />
        {{__('hybridcms::navigation.users.create')}}
      </a>
    </li>

    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown mt-auto">
      <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="">
        <x-hybridcms-svg svg="settings" class="c-sidebar-nav-icon" viewBox="512 512" />
        {{__('hybridcms::navigation.user.dropdown.settings')}}</a>
        <ul class="c-sidebar-nav-dropdown-items">
          <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{route('languages.index')}}">
              {{__('hybridcms::navigation.languages.settings')}}
            </a>
          </li>
        </ul>
    </li>
    @endcan
  </ul>
  <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
    data-class="c-sidebar-minimized"></button>
</div>